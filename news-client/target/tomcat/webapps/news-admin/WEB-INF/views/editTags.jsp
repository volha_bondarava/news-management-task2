<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<title>HomePage</title>
<meta charset="utf-8">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<div class="col-sm-9">
<sf:form id="updateForm" method="POST" action="/news-admin/updateTag" modelAttribute="tagUpdate">
        <sf:hidden id="updateName" path="name"/>
        <sf:hidden id="updateId" path="id"/>
        <sf:errors id="updateName" path="name" cssClass="error"/>
    </sf:form>
    <div class="panel panel-primary">
        <div class="panel-body">
            <c:forEach items="${tagList}" var="tag">

                <div class="row form-group">


                    <div class="col-sm-2">
                        <spring:message code="tag"/>
                    </div>

                    <div class="col-sm-4">
                        <input class="form-control" path="fullText" size="15" id="${tag.id}" name="name"
                               value="${tag.name}" readonly="readonly"/>
                    </div>
                       <div class="col-sm-2" id="edit${tag.id}">
                                            <input type="submit"
                                                   onClick="edit('${tag.id}')"
                                                   class="btn btn-link" value="<spring:message code="edit"/>"/>
                                        </div>

                    <div class="col-sm-2" id="update${tag.id}" style="display:none;">
                        <button class="btn btn-link" onClick="update('${tag.id}')">
                            <spring:message code="update"/>
                        </button>
                    </div>


                        <div class="col-sm-2" id="expire${tag.id}" style="display:none;">
                            <form method="POST" action="/news-admin/deleteTag">
                                <input type="submit" class="btn btn-link" value="<spring:message code="delete"/>"/>
                                <input type="hidden" name="tagId" value="${tag.id}">
                            </form>
                        </div>

                        <div class="col-sm-2" id="cancel${tag.id}" style="display:none;">
                            <input type="submit"
                                   onClick="edit('${tag.id}')"
                                   class="btn btn-link" value="<spring:message code="cancel"/>"/>
                        </div>

                </div>

            </c:forEach>
            <div class="row">
                <div class="col-sm-2">
                    <spring:message code="addTag"/>
                </div>

                <sf:form class="col-sm-7" method="POST" action="/news-admin/addTag" modelAttribute="tagForm">
                    <div class="col-sm-10">
                        <sf:input class="form-control" path="name"/>
                        <sf:errors path="name" cssClass="error"/>
                    </div>
                     <div class="col-sm-2"></div>
                    <div class="col-sm-2">
                        <input type="submit" class="btn btn-link" value="<spring:message code="save"/>"/>
                    </div>
                </sf:form>
            </div>
        </div>
    </div>
</div>

