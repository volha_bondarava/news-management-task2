<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="col-sm-3">
    <ul class="nav nav-pills nav-stacked">

        <li>
            <form method="GET">
                <a href="/news-admin/newsList/1"><spring:message code="newsList"/></a>
            </form>
        </li>
        <li>
            <form method="GET">
               <a href="/news-admin/toAddNews"><spring:message code="addNews"/></a>
            </form>
        </li>
        <li>
            <form method="GET">
                 <a href="/news-admin/toEditAuthors"><spring:message code="editAuthors"/></a>
            </form>
        </li>

        <li>
            <form method="GET">
                 <a href="/news-admin/toEditTags"><spring:message code="editTags"/></a>
            </form>
        </li>
    </ul>
</div>