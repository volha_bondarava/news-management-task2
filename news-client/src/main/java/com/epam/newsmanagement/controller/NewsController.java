package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.FilteredNews;
import com.epam.newsmanagement.entity.NewsDTO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class NewsController {
    @Autowired
    private INewsService newsService;
    @Autowired
    private IAuthorService authorService;
    @Autowired
    private INewsManagementService newsManagementService;
    @Autowired
    private ITagService tagService;

    private static final int START_NUM = 0;
    private static final int NEWS_PER_PAGE = 4;
    private static final int START_POSITION = 1;
    private static final int NEWS_COUNT_ZERO = 0;

    @RequestMapping(value = "/newsList/{page}", method = RequestMethod.GET)
    public String newsListPerPage(@PathVariable int page, HttpServletRequest request, Model model) throws ServiceException {
        model.addAttribute("authorList", authorService.getAuthorList());
        model.addAttribute("tagList", tagService.getTagList());
        HttpSession session = request.getSession();
        session.setAttribute("pageNum", page);
        SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("filter");
        FilteredNews filteredNews = newsManagementService.getFilteredDTOList((page - 1) * NEWS_PER_PAGE, NEWS_PER_PAGE * (page-1) + NEWS_PER_PAGE, searchCriteria);
        model.addAttribute("newsDTOList", filteredNews.getNewsDTOList());
        model.addAttribute("pageCount", countPages(filteredNews.getMatchedCount()));
        if(searchCriteria == null){
            model.addAttribute("criteriaForm", new SearchCriteria());}
        else {
            model.addAttribute("criteriaForm", searchCriteria);
        }
        return "newsList";
    }

    @RequestMapping(value = "/reset", method = RequestMethod.GET)
    public String reset(HttpServletRequest request, Model model) throws ServiceException {
        model.addAttribute("authorList", authorService.getAuthorList());
        model.addAttribute("tagList", tagService.getTagList());
        HttpSession session = request.getSession();
        session.setAttribute("filter", null);
        FilteredNews filteredNews = newsManagementService.getFilteredDTOList(START_NUM, NEWS_PER_PAGE, new SearchCriteria());
        model.addAttribute("newsDTOList", filteredNews.getNewsDTOList());
        model.addAttribute("pageCount", countPages(filteredNews.getMatchedCount()));
        model.addAttribute("criteriaForm", new SearchCriteria());
        return "newsList";
    }

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String home() throws ServiceException {
        return "redirect:/newsList/" + 1;
    }


    @RequestMapping(value = "/getNewsPosition", method = RequestMethod.POST)
    public String getNewsPosition(@RequestParam("newsId") Long newsId, @RequestParam("position") int position,
                                  HttpServletRequest request, Model model) throws ServiceException {
        HttpSession session = request.getSession();
        session.setAttribute("position", position);
        return "redirect:/news/" + newsId;
    }

    @RequestMapping(value = "/filteredNewsList", method = RequestMethod.POST)
    public String getFilteredNewsList(HttpServletRequest request, @ModelAttribute("criteriaForm") SearchCriteria searchCriteria, Model model) throws ServiceException {
        HttpSession session = request.getSession();
        searchCriteria.setAuthorId(searchCriteria.getAuthorId());
        List<Long> tagList = null;
        if(searchCriteria.getTagsId()!=null){
            tagList = new ArrayList<>();
            for(Long currentTag: searchCriteria.getTagsId()){
                tagList.add(currentTag);
            }
        }
        searchCriteria.setTagsId(tagList);
        session.setAttribute("filter", searchCriteria);
        FilteredNews filteredNews = newsManagementService.getFilteredDTOList(START_NUM, NEWS_PER_PAGE, searchCriteria);
        model.addAttribute("newsDTOList", filteredNews.getNewsDTOList());
        model.addAttribute("pageCount", countPages(filteredNews.getMatchedCount()));
        model.addAttribute("authorList", authorService.getAuthorList());
        model.addAttribute("tagList", tagService.getTagList());
        model.addAttribute("criteriaForm", searchCriteria);
        return "newsList";
    }


    @RequestMapping(value = "/news/{newsId}", method = RequestMethod.GET)
    public String newsById(@PathVariable Long newsId,HttpServletRequest request, Model model) throws ServiceException {
        HttpSession session = request.getSession();
        model.addAttribute("newsDTO", newsManagementService.getNewsDTOByNewsId(newsId));
        int position = (int)session.getAttribute("position");
        int pageNum = (int)session.getAttribute("pageNum");
        SearchCriteria searchCriteria = (SearchCriteria)session.getAttribute("filter");
        int newsCount = countCurrentNews(position, pageNum);
        if(newsCount == NEWS_COUNT_ZERO){
            model.addAttribute("isFirst", 1);
        }
        if(newsCount == newsService.countNews(searchCriteria)-1){
            model.addAttribute("isLast", 1);
        }
        Comment comment = new Comment();
        comment.setNewsId(newsId);
        model.addAttribute("commentForm", comment);
        return "news";
    }

    @RequestMapping(value = "/currentNews/{newsCount}", method = RequestMethod.GET)
    public String currentNews(@PathVariable int newsCount, Model model, HttpServletRequest request) throws ServiceException {
        HttpSession session = request.getSession();
        SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("filter");
        NewsDTO newsDTO = newsManagementService.getCurrentNewsDTO(searchCriteria, newsCount);
        model.addAttribute("newsDTO", newsDTO);
        if(newsCount == NEWS_COUNT_ZERO){
            model.addAttribute("isFirst", 1);
        }
        if(newsCount == newsService.countNews(searchCriteria)-1){
            model.addAttribute("isLast", 1);
        }
        Comment comment = new Comment();
        comment.setNewsId(newsDTO.getNews().getId());
        model.addAttribute("commentForm", comment);
        return "news";
    }


    @RequestMapping(value = "/prevNews", method = RequestMethod.GET)
    public String prevNews(Model model, HttpServletRequest request) throws ServiceException {
        HttpSession session = request.getSession();
        int position = (int) session.getAttribute("position");
        int pageNum = (int) session.getAttribute("pageNum");
        int newsCount = countPrevNews(position, pageNum);

        if (position == START_POSITION) {
            session.setAttribute("position", NEWS_PER_PAGE);
            int currentPageNum = (int) session.getAttribute("pageNum");
            --currentPageNum;
            session.setAttribute("pageNum", currentPageNum);
        } else {
            int currentPosition = (int) session.getAttribute("position");
            --currentPosition;
            session.setAttribute("position", currentPosition);
        }
        return "redirect:/currentNews/" + newsCount;
    }

    @RequestMapping(value = "/nextNews", method = RequestMethod.GET)
    public String nextNews(Model model, HttpServletRequest request) throws ServiceException {
        HttpSession session = request.getSession();
        int position = (int) session.getAttribute("position");
        int pageNum = (int) session.getAttribute("pageNum");
        int newsCount = countNextNews(position, pageNum);
        if (position == NEWS_PER_PAGE) {
            session.setAttribute("position", START_POSITION);
            int currentPageNum = (int) session.getAttribute("pageNum");
            ++currentPageNum;
            session.setAttribute("pageNum", currentPageNum);
        } else {
            int currentPosition = (int) session.getAttribute("position");
            ++currentPosition;
            session.setAttribute("position", currentPosition);
        }
        return "redirect:/currentNews/" + newsCount;
    }

    private static int countPages(int newsCount) {
        return (newsCount - 1) / NEWS_PER_PAGE + 1;
    }

    private static int countPrevNews(int position, int pageNum) {
        return (pageNum - 1) * NEWS_PER_PAGE + position - 2;
    }

    private static int countNextNews(int position, int pageNum) {
        return (pageNum - 1) * NEWS_PER_PAGE + position;
    }

    private static int countCurrentNews(int position, int pageNum) {
        return (pageNum - 1) * NEWS_PER_PAGE + position - 1;
    }
}