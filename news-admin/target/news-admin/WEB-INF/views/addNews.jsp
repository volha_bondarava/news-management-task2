<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>

<div class="col-sm-9">
    <div class="panel panel-primary">
        <div class="panel-body">
            <sf:form method="POST" action="/news-admin/addNews" modelAttribute="newsForm">
             <sf:hidden class="form-control" path="newsId"/>
                <div class="row">
                    <div class="col-sm-2">
                    <spring:message code="title"/>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <sf:input class="form-control" path="title" size="15"/>
                            <sf:errors path="title" cssClass="error"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                            <spring:message code="brief"/>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <sf:input class="form-control" path="shortText" size="15"/>
                            <sf:errors path="shortText" cssClass="error"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                            <spring:message code="content"/>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <sf:input class="form-control" path="fullText" size="15"/>
                            <sf:errors path="fullText" cssClass="error"/>
                        </div>
                    </div>
                </div>

                 <div class="row">
                    <div class="col-sm-2">
                            <spring:message code="date"/>
                    </div>
                    <div class="col-sm-6">
                       <div class="form-group">

                    <sf:input type="text"  class="form-control" path="modificationDate"/>
                                           </div>

                  <!--<div class="form-group">-->
                      <!--<spring:message var="datePattern" code="datePattern"></spring:message>-->
                      <!--${newsDTO.news.creationDate}-->
                      <!--<input type="text"  class="form-control" name="date"-->
                             <!--value="<fmt:formatDate type="date" pattern="${datePattern}" value="${date}"/>">-->

                  <!--</div>-->
                    </div>
                           <div class="col-sm-2">
                                                                   <spring:message code="datePattern"/>
                                                                   </div>
                </div>

                <div class="row">
                     <div class="col-sm-3">
                     <sf:select path="tagList" multiple="true" size="1" class="form-control selectpicker">
                     <sf:options items="${tagList}" itemValue="id" itemLabel="name"/>
                     </sf:select>

                     </div>
                    <div class="col-sm-3">
                        <sf:select path="authorId" size="1" class="form-control selectpicker">
                            <sf:options items="${authorList}" itemValue="id" itemLabel="name"/>
                        </sf:select>

                    </div>

                </div>
           <br>

                <input type="submit" class="btn btn-primary btn-lg" value="<spring:message code="save"/>"/>
            </sf:form>

        </div>
        ${hint}
    </div>
</div>



