<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="col-sm-9">
    <h2><spring:message code="login"/></h2>
    <form method="post" class="signin" action='<c:url value='/j_spring_security_check' />'>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label><spring:message code="email"/></label>
                    <input required name="j_username" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label><spring:message code="password"/></label>
                    <input type="password" required name="j_password" class="form-control">
                </div>
            </div>
        </div>
        <div>
        <input id="remember_me"
         name="_spring_security_remember_me" type="checkbox"/>
         <label for="remember_me"
         class="inline"><spring:message code="rememberMe"/></label>
         </div>
        <button type="submit" class="btn btn-primary btn-lg"><spring:message code="login"/></button>

    </form>


</div>