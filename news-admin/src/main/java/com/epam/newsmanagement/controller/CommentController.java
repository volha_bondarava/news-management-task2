package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.entity.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Date;

@Controller
public class CommentController {
    @Autowired
    private ICommentService commentService;

    @RequestMapping(value = "/addComment", method = RequestMethod.POST)
    public String addComment(@Valid @ModelAttribute("commentForm") Comment comment1,BindingResult bindingResult, Model model) throws ServiceException {
        if (bindingResult.hasErrors()) {
            return "redirect:/news/" + comment1.getNewsId();
        } else {
            Comment comment = new Comment();
            comment.setText(comment1.getText());
            comment.setCreationDate(new Date());
            comment.setNewsId(comment1.getNewsId());
            commentService.addComment(comment);

            return "redirect:/news/" + comment1.getNewsId();
        }
    }

    @RequestMapping(value = "/deleteComment", method = RequestMethod.POST)
    public String deleteComment(@RequestParam("commentId") Long commentId,
                                @RequestParam("newsId") Long newsId, Model model) throws ServiceException {
        commentService.deleteComment(commentId);
        Comment comment = new Comment();
        comment.setNewsId(newsId);
        model.addAttribute("commentForm", comment);
        return "redirect:/news/" + newsId;
    }
}
