package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class TagController {
    @Autowired
    private ITagService tagService;

    @RequestMapping(value = "/toEditTags", method = RequestMethod.GET)
    public String toEditTagsGET(Model model) throws ServiceException {
        model.addAttribute("tagList", tagService.getTagList());
        model.addAttribute("tagForm", new Tag());
        model.addAttribute("tagUpdate", new Tag());
        return "editTags";
    }

    @RequestMapping(value = "/deleteTag", method = RequestMethod.POST)
    public String deleteTag(@RequestParam("tagId") Long tagId, Model model) throws ServiceException {
        tagService.deleteTag(tagId);
        return "redirect:/toEditTags";
    }

    @RequestMapping(value = "/addTag", method = RequestMethod.POST)
    public String addTag(@Valid @ModelAttribute("tagForm") Tag tag, BindingResult bindingResult,@ModelAttribute("tagUpdate") Tag tag1, Model model) throws ServiceException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("tagList", tagService.getTagList());
            return "editTags";
        } else {
            tagService.addTag(tag);
            return "redirect:/toEditTags";
        }
    }

    @RequestMapping(value = "/updateTag", method = RequestMethod.POST)
    public String updateTag(@Valid @ModelAttribute("tagUpdate") Tag tag,BindingResult bindingResult,  @ModelAttribute("tagForm") Tag tag1, Model model) throws ServiceException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("tagList", tagService.getTagList());
            return "editTags";
        } else {
            tagService.updateTag(tag);
            return "redirect:/toEditTags";
        }
    }
}
