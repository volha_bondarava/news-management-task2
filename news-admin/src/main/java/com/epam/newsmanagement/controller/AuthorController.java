package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Date;

@Controller
public class AuthorController {
    @Autowired
    private IAuthorService authorService;

    @RequestMapping(value = "/toEditAuthors", method = RequestMethod.GET)
    public String toEditAuthorsGET(Model model) {
        model.addAttribute("authorForm", new Author());
        model.addAttribute("authorUpdate", new Author());
        try {
            model.addAttribute("authorList", authorService.getAuthorList());
        } catch (ServiceException e) {
            return "error";
        }
        return "editAuthors";
 }

    @RequestMapping(value = "/addAuthor", method = RequestMethod.POST)
    public String addAuthor(@Valid @ModelAttribute("authorForm") Author author, BindingResult bindingResult,
                            @ModelAttribute("authorUpdate") Author author1, Model model) throws ServiceException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("authorList", authorService.getAuthorList());
            return "editAuthors";
        } else {
            authorService.addAuthor(author);
            return "redirect:/toEditAuthors";
        }
    }

    @RequestMapping(value = "/expireAuthor", method = RequestMethod.POST)
    public String expireAuthor(@RequestParam("authorId") Long authorId, Model model) throws ServiceException {
        authorService.expireAuthor(authorId, new Date());
        return "redirect:/toEditAuthors";
    }

    @RequestMapping(value = "/updateAuthor", method = RequestMethod.POST)
    public String updateAuthor(@Valid @ModelAttribute("authorUpdate") Author author,BindingResult bindingResult,  @ModelAttribute("authorForm") Author author1, Model model) throws ServiceException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("authorList", authorService.getAuthorList());
            return "editAuthors";
        } else {
            authorService.updateAuthor(author);
            return "redirect:/toEditAuthors";
        }
    }
}
