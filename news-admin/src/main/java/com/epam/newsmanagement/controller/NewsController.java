package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.INewsManagementService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.view.NewsView;
import com.epam.newsmanagement.editor.DateEditor;
import com.epam.newsmanagement.exception.ServiceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.*;

@Controller
public class NewsController {
    @Autowired
    private INewsService newsService;
    @Autowired
    private IAuthorService authorService;
    @Autowired
    private INewsManagementService newsManagementService;
    @Autowired
    private ITagService tagService;
    @Autowired
    private DateEditor dateEditor;

    private static final int START_NUM = 0;
    private static final int NEWS_PER_PAGE = 4;
    private static final int START_POSITION = 1;
    private static final int NEWS_COUNT_ZERO = 0;

    @InitBinder
    public void initBinderForDate(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, dateEditor);
    }

    @RequestMapping(value = "/toNewsList/{page}", method = RequestMethod.POST)
    public String toNewsList(Model model) throws ServiceException {
        return "redirect:/newsList/1";
    }

    @RequestMapping(value = "/newsList/{page}", method = RequestMethod.GET)
    public String newsListPerPage(@PathVariable int page, HttpServletRequest request, Model model) throws ServiceException {
        model.addAttribute("authorList", authorService.getAuthorList());
        model.addAttribute("tagList", tagService.getTagList());
        HttpSession session = request.getSession();
        session.setAttribute("pageNum", page);
        SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("filter");
            FilteredNews filteredNews = newsManagementService.getFilteredDTOList((page - 1) * NEWS_PER_PAGE, NEWS_PER_PAGE * (page-1) + NEWS_PER_PAGE, searchCriteria);
            model.addAttribute("newsDTOList", filteredNews.getNewsDTOList());
            model.addAttribute("pageCount", countPages(filteredNews.getMatchedCount()));
        if(searchCriteria == null){
        model.addAttribute("criteriaForm", new SearchCriteria());}
        else {
            model.addAttribute("criteriaForm", searchCriteria);
        }
        return "newsList";
    }

    @RequestMapping(value = "/reset", method = RequestMethod.GET)
    public String reset(HttpServletRequest request, Model model) throws ServiceException {
        model.addAttribute("authorList", authorService.getAuthorList());
        model.addAttribute("tagList", tagService.getTagList());
        HttpSession session = request.getSession();
        session.setAttribute("filter", null);
        FilteredNews filteredNews = newsManagementService.getFilteredDTOList(START_NUM, NEWS_PER_PAGE, new SearchCriteria());
        model.addAttribute("newsDTOList", filteredNews.getNewsDTOList());
        model.addAttribute("pageCount", countPages(filteredNews.getMatchedCount()));
        model.addAttribute("criteriaForm", new SearchCriteria());
        return "newsList";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model) throws ServiceException {
        return "login";
    }


    @RequestMapping(value = "/getNewsPosition", method = RequestMethod.POST)
    public String getNewsPosition(@RequestParam("newsId")Long newsId, @RequestParam("position")int position,
                                  HttpServletRequest request, Model model) throws ServiceException {
        HttpSession session = request.getSession();
        session.setAttribute("position", position);
        return "redirect:/news/"+newsId;
    }

    @RequestMapping(value = "/toUpdateNews/{newsId}", method = RequestMethod.GET)
    public String toUpdateNews(@PathVariable Long newsId, Model model) throws ServiceException {
        model.addAttribute("authorList", authorService.getAuthorList());
        model.addAttribute("tagList", tagService.getTagList());
        model.addAttribute("newsForm", parseNewsDTO(newsManagementService.getNewsDTOByNewsId(newsId)));
        return "updateNews";
    }

    @RequestMapping(value = "/updateNews", method = RequestMethod.POST)
    public String updateNews(@Valid @ModelAttribute("newsForm") NewsView newsView, BindingResult bindingResult, Model model) throws ServiceException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("authorList", authorService.getAuthorList());
            model.addAttribute("tagList", tagService.getTagList());
            return "updateNews";
        } else {
            NewsDTO newsDTO = new NewsDTO();
            parseNewsView(newsDTO, newsView);
            newsManagementService.updateNewsDTO(newsDTO);
            model.addAttribute("hint", "News has been updated");
            return "updateNews";
        }
    }

    @RequestMapping(value = "/filteredNewsList", method = RequestMethod.POST)
    public String getFilteredNewsList(HttpServletRequest request, @ModelAttribute("criteriaForm") SearchCriteria searchCriteria, Model model) throws ServiceException {
        HttpSession session = request.getSession();
        searchCriteria.setAuthorId(searchCriteria.getAuthorId());
        List<Long> tagList = null;
        if(searchCriteria.getTagsId()!=null){
            tagList = new ArrayList<>();
        for(Long currentTag: searchCriteria.getTagsId()){
            tagList.add(currentTag);
        }
        }
        searchCriteria.setTagsId(tagList);
        session.setAttribute("filter", searchCriteria);
        FilteredNews filteredNews = newsManagementService.getFilteredDTOList(START_NUM, NEWS_PER_PAGE, searchCriteria);
        model.addAttribute("newsDTOList", filteredNews.getNewsDTOList());
        model.addAttribute("pageCount", countPages(filteredNews.getMatchedCount()));
        model.addAttribute("authorList", authorService.getAuthorList());
        model.addAttribute("tagList", tagService.getTagList());
        model.addAttribute("criteriaForm", searchCriteria);
        return "newsList";
    }

    @RequestMapping(value = "/toAddNews", method = RequestMethod.GET)
    public String toAddNews(Model model) throws ServiceException {
        model.addAttribute("authorList", authorService.getAuthorList());
        model.addAttribute("tagList", tagService.getTagList());
        model.addAttribute("newsForm", new NewsView());
        return "addNews";
    }

    @RequestMapping(value = "/deleteNews", method = RequestMethod.POST)
    public String deleteNews(Model model, @RequestParam("toDelete") String[] toDelete) throws ServiceException {
        for (String newsToDelete : toDelete) {
            newsManagementService.deleteNews(Long.parseLong(newsToDelete));
        }
        return "redirect:/newsList/1";
    }


    @RequestMapping(value = "/addNews", method = RequestMethod.POST)
    public String addNews(@Valid @ModelAttribute("newsForm") NewsView newsView, BindingResult bindingResult, Model model) throws ServiceException {
        if (bindingResult.hasErrors()) {
            model.addAttribute("authorList", authorService.getAuthorList());
            model.addAttribute("tagList", tagService.getTagList());
            return "addNews";
        } else {
            NewsDTO newsDTO = new NewsDTO();
            parseNewsView(newsDTO, newsView);
            newsManagementService.addNewsDTO(newsDTO);
            return "redirect:/toAddNews";
        }
    }

    @RequestMapping(value = "/news/{newsId}", method = RequestMethod.GET)
    public String newsById(@PathVariable Long newsId,HttpServletRequest request, Model model) throws ServiceException {
        HttpSession session = request.getSession();
        model.addAttribute("newsDTO", newsManagementService.getNewsDTOByNewsId(newsId));
        int position = (int)session.getAttribute("position");
        int pageNum = (int)session.getAttribute("pageNum");
        SearchCriteria searchCriteria = (SearchCriteria)session.getAttribute("filter");
        int newsCount = countCurrentNews(position, pageNum);
        if(newsCount == NEWS_COUNT_ZERO){
            model.addAttribute("isFirst", 1);
        }
        if(newsCount == newsService.countNews(searchCriteria)-1){
            model.addAttribute("isLast", 1);
        }
        Comment comment = new Comment();
        comment.setNewsId(newsId);
        model.addAttribute("commentForm", comment);
        return "news";
    }

    @RequestMapping(value = "/currentNews/{newsCount}", method = RequestMethod.GET)
    public String currentNews(@PathVariable int newsCount, Model model, HttpServletRequest request) throws ServiceException {
        HttpSession session = request.getSession();
        SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("filter");
        NewsDTO newsDTO = newsManagementService.getCurrentNewsDTO(searchCriteria, newsCount);
        model.addAttribute("newsDTO", newsDTO);
        if(newsCount == NEWS_COUNT_ZERO){
            model.addAttribute("isFirst", 1);
        }
        if(newsCount == newsService.countNews(searchCriteria)-1){
            model.addAttribute("isLast", 1);
        }
        Comment comment = new Comment();
        comment.setNewsId(newsDTO.getNews().getId());
        model.addAttribute("commentForm", comment);
        return "news";
    }

    @RequestMapping(value = "/prevNews", method = RequestMethod.GET)
    public String prevNews(Model model, HttpServletRequest request) throws ServiceException {
        HttpSession session = request.getSession();
        int position = (int)session.getAttribute("position");
        int pageNum = (int)session.getAttribute("pageNum");
        int newsCount = countPrevNews(position, pageNum);

        if(position == START_POSITION){
            session.setAttribute("position", NEWS_PER_PAGE);
            int currentPageNum =(int)session.getAttribute("pageNum");
            --currentPageNum;
            session.setAttribute("pageNum", currentPageNum);
        }
        else {
            int currentPosition =(int) session.getAttribute("position");
            --currentPosition;
            session.setAttribute("position", currentPosition);
        }
        return "redirect:/currentNews/"+newsCount;
    }

    @RequestMapping(value = "/nextNews", method = RequestMethod.GET)
    public String nextNews(Model model, HttpServletRequest request) throws ServiceException {
        HttpSession session = request.getSession();
        int position = (int)session.getAttribute("position");
        int pageNum = (int)session.getAttribute("pageNum");
        int newsCount = countNextNews(position, pageNum);
        if(position == NEWS_PER_PAGE){
            session.setAttribute("position", START_POSITION);
            int currentPageNum =(int)session.getAttribute("pageNum");
            ++currentPageNum;
            session.setAttribute("pageNum", currentPageNum);
        }
        else {
            int currentPosition =(int)session.getAttribute("position");
            ++currentPosition;
            session.setAttribute("position", currentPosition);
        }
        return "redirect:/currentNews/"+newsCount;
    }

    @RequestMapping(value = "/logOut", method = RequestMethod.GET)
    public String logOut(Model model, HttpServletRequest request) throws ServiceException {
        request.getSession().invalidate();
        return "login";
    }

    @Secured("ROLE_ADMIN")
    private static int countPages(int newsCount){
        return (newsCount - 1)/NEWS_PER_PAGE + 1;
    }

    @Secured("ROLE_ADMIN")
    private static int countPrevNews(int position, int pageNum){
        return (pageNum - 1) * NEWS_PER_PAGE + position - 2;
    }

    @Secured("ROLE_ADMIN")
    private static int countNextNews(int position, int pageNum){
        return (pageNum - 1) * NEWS_PER_PAGE + position;
    }

    @Secured("ROLE_ADMIN")
    private static int countCurrentNews(int position, int pageNum){
        return (pageNum - 1) * NEWS_PER_PAGE + position - 1;
    }

    @Secured("ROLE_ADMIN")
    private void parseNewsView(NewsDTO newsDTO, NewsView newsView) {
        News news = new News();
        news.setId(newsView.getNewsId());
        news.setModificationDate(newsView.getModificationDate());
        news.setCreationDate(newsView.getModificationDate());
        news.setShortText(newsView.getShortText());
        news.setTitle(newsView.getTitle());
        news.setFullText(newsView.getFullText());
        newsDTO.setNews(news);

        Author author = new Author();
        author.setId(newsView.getAuthorId());
        newsDTO.setAuthor(author);

        List<Tag> tagList = new ArrayList<>();
        Tag tag;
        for(String tagId:newsView.getTagList()){
            tag = new Tag();
            tag.setId(Long.parseLong(tagId));
            tagList.add(tag);
        }
        newsDTO.setTagList(tagList);
    }

    private NewsView parseNewsDTO(NewsDTO newsDTO) {
        NewsView newsView = new NewsView();
        newsView.setFullText(newsDTO.getNews().getFullText());
        newsView.setTitle(newsDTO.getNews().getTitle());
        newsView.setShortText(newsDTO.getNews().getShortText());
        newsView.setModificationDate(newsDTO.getNews().getModificationDate());
        newsView.setAuthorId(newsDTO.getAuthor().getId());
        newsView.setNewsId(newsDTO.getNews().getId());
        List<Tag> tagList = newsDTO.getTagList();
        String [] tagsId = new String[tagList.size()];
        int i = 0;
        for(Tag tag: tagList){
            tagsId[i++] = String.valueOf(tag.getId());
        }
        newsView.setTagList(tagsId);
        return newsView;
    }
}