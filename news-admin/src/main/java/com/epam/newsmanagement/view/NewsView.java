package com.epam.newsmanagement.view;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

public class NewsView implements Serializable{
    private static final long serialVersionUID = -9127426285320045783L;
    String[] tagList;
    Long authorId;
    Long newsId;
    @NotNull
    @Size(min=3, max=100,
            message="Short message must be between 3 and 20 characters long.")
    private String shortText;
    @NotNull
    @Size(min=20, max=2000,
            message="Full message must be between 20 and 2000 characters long.")
    private String fullText;
    @NotNull
    @Size(min=3, max=30,
            message="Title must be between 3 and 20 characters long.")
    private String title;
    @NotNull
    private Date modificationDate;

    public NewsView() {
    }

    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public String[] getTagList() {
        return tagList;
    }

    public void setTagList(String[] tagList) {
        this.tagList = tagList;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NewsView)) return false;

        NewsView newsView = (NewsView) o;

        if (!Arrays.equals(getTagList(), newsView.getTagList())) return false;
        if (getAuthorId() != null ? !getAuthorId().equals(newsView.getAuthorId()) : newsView.getAuthorId() != null)
            return false;
        if (getNewsId() != null ? !getNewsId().equals(newsView.getNewsId()) : newsView.getNewsId() != null)
            return false;
        if (getShortText() != null ? !getShortText().equals(newsView.getShortText()) : newsView.getShortText() != null)
            return false;
        if (getFullText() != null ? !getFullText().equals(newsView.getFullText()) : newsView.getFullText() != null)
            return false;
        if (getTitle() != null ? !getTitle().equals(newsView.getTitle()) : newsView.getTitle() != null) return false;
        return !(getModificationDate() != null ? !getModificationDate().equals(newsView.getModificationDate()) : newsView.getModificationDate() != null);

    }

    @Override
    public int hashCode() {
        int result = getTagList() != null ? Arrays.hashCode(getTagList()) : 0;
        result = 31 * result + (getAuthorId() != null ? getAuthorId().hashCode() : 0);
        result = 31 * result + (getNewsId() != null ? getNewsId().hashCode() : 0);
        result = 31 * result + (getShortText() != null ? getShortText().hashCode() : 0);
        result = 31 * result + (getFullText() != null ? getFullText().hashCode() : 0);
        result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
        result = 31 * result + (getModificationDate() != null ? getModificationDate().hashCode() : 0);
        return result;
    }
}
