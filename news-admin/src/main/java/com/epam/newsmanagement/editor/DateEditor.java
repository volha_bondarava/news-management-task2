package com.epam.newsmanagement.editor;

import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Component
public class DateEditor extends PropertyEditorSupport {
    @Override
    public void setAsText(String text) {
        String stringDateFormat = "yyyy-MM-dd";
        SimpleDateFormat format = new SimpleDateFormat(stringDateFormat, Locale.US);
        Date date = null;
        try {
            date = format.parse(text);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.setValue(date);
    }

}