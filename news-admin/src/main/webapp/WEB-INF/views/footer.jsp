<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<style>
   html {
        position: relative;
        min-height: 100%;
    }

    body {
        margin-bottom: 60px;
    }

    .footer {
        position: absolute;
        bottom: 0;
        width: 100%;
        height: 60px;
        background-color: #5bc0de;
        text-align: center;
    }
</style>
<footer class="footer">

        <p class="text-muted"><span class="glyphicon glyphicon-copyright-mark"></span> Epam 2015. <spring:message code="copyright"/>
        </p>
</footer>
