<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>

<div class="col-sm-9">
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
              <form id="positionForm" method="POST" action="/news-admin/getNewsPosition">
                    <input type="hidden" id="newsId" name="newsId"/>
                    <input type="hidden" id="position" name="position"/>
                </form>
                <div class="col-sm-2">
                    <form method="GET" action="/news-admin/reset">
                        <input type="submit" class="btn btn-primary" value="<spring:message code="reset"/>">
                    </form>
                </div>
                  <sf:form method="POST" action="/news-admin/filteredNewsList" modelAttribute="criteriaForm">
                    <div class="col-sm-3">
                        <input type="submit" class="btn btn-primary" value="<spring:message code="filter"/>">
                    </div>
                    <div class="col-sm-4">
                                            <sf:select path="tagsId" multiple="true" class="selectpicker">

                                                                         <sf:options items="${tagList}" itemValue="id" itemLabel="name"/>

                                                                 </sf:select>
                                        </div>


                    <div class="col-sm-3">
                        <sf:select path="authorId" size="1" class="form-control selectpicker">
                            <sf:option value="">Nothing selected</sf:option>
                            <c:forEach items="${authorList}" var="author">
                                <sf:option value="${author.id}">${author.name}</sf:option>
                            </c:forEach>
                        </sf:select>
                    </div>
                </sf:form>
            </div>
            <form  method="POST" action="/news-admin/deleteNews">
                <c:forEach items="${newsDTOList}" var="newsDTO" varStatus="status">
                    <div class="row">
                        <div class="col-sm-8">
                        <div class="btn btn-link" onClick="sendNewsPosition('${newsDTO.news.id}','${status.count}')">${newsDTO.news.title}</div>
                        </div>
                        <div class="col-sm-2">
                                ${newsDTO.author.name}
                        </div>
                        <div class="col-sm-2">
                        <spring:message var="datePattern" code="datePattern"></spring:message>

                    <fmt:formatDate type="date" pattern="${datePattern}" value="${newsDTO.news.creationDate}"/>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                                ${newsDTO.news.shortText}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-5">
                            <c:forEach items="${newsDTO.tagList}" var="tag">
                                ${tag.name}
                            </c:forEach>
                        </div>
                        <div class="col-sm-4">
                           <spring:message code="comment"/> ${newsDTO.commentList.size()}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10"></div>
                        <div class="col-sm-1">
                            <a href="/news-admin/toUpdateNews/${newsDTO.news.id}">Edit</a>
                        </div>
                        <div class="col-sm-1">
                            <input type="checkbox" class="checkbox" name="toDelete" value="${newsDTO.news.id}">
                        </div>
                    </div>

                </c:forEach>
                <div class="row">
                    <div class="col-sm-10"></div>
                    <div class="col-sm-2">
                <input type="submit" class="btn btn-primary" id="submitBut" disabled value="<spring:message code="delete"/>">
                    </div>
                </div>
            </form>
        </div>


    </div>
<c:if test="${pageCount > 1}">
        <ul class="pagination">
            <div class="row">
                <c:forEach var="page" begin="1" end="${pageCount}">
                    <div class="col-sm-1">
                         <a href="/news-admin/newsList/${page}">${page}</a>
                    </div>
                </c:forEach>
            </div>
        </ul>
</c:if>
</div>