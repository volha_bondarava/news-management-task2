 function edit(id) {
        if (document.getElementById(id).readOnly == false) {
            document.getElementById(id).readOnly = true;
              document.getElementById('update'+id).style.display = "none";
              document.getElementById('change' + id).style.display = "none";
              document.getElementById('edit' + id).style.display = "block";
        }
        else {
            document.getElementById('edit' + id).style.display = "none";
            document.getElementById('update'+id).style.display = "block";
            document.getElementById('change' + id).style.display = "block";
            document.getElementById(id).readOnly = false;
        }
}