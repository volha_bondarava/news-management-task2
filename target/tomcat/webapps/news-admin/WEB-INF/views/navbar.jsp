<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="col-sm-3">
    <ul class="nav nav-pills nav-stacked">
        <li>
            <form method="POST"
                  action="/news-admin/newsList">
                <button type="submit" class="btn btn-link">News List</button>

            </form>
        </li>
        <li>
            <form method="POST"
                  action="/news-admin/toAddNews">
                <button type="submit" class="btn btn-link">Add News</button>

            </form>
        </li>
        <li>
            <form method="POST"
                  action="/news-admin/toEditAuthors">
                <button type="submit" class="btn btn-link">Add/Update Authors</button>

            </form>
        </li>
        <li>
            <form method="POST"
                  action="/news-admin/toEditTags">
                <button type="submit" class="btn btn-link">Add/Update Tags</button>

            </form>
        </li>
    </ul>
</div>