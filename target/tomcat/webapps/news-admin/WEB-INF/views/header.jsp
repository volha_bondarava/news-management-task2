     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
     <%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
     <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
     <%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
     <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
     <%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <div class="navbar-brand"><spring:message code="siteName"/></div>
    </div>

      <ul class="nav navbar-nav navbar-right">
      <li><a href="?lang=en"><spring:message code="EN"/></a></li>
      <li><a href="?lang=ru"><spring:message code="RU"/></a></li>
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> <spring:message code="logout"/></a></li>
      </ul>
  </div>
</nav>