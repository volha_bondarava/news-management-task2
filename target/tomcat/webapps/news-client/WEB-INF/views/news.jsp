<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false"%>
<div class="col-sm-9">
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-8">${newsDTO.news.title}</div>
                <div class="col-sm-2">${newsDTO.author.name}</div>
                <div class="col-sm-2">${newsDTO.news.creationDate}</div>
            </div>

            ${newsDTO.news.fullText}
            <c:forEach items="${newsDTO.commentList}" var="comment">
                <form method="POST" action="/news-admin/deleteComment">
                    <input type="hidden" name="commentId" value="${comment.id}">
                    <input type="hidden" name="newsId" value="${newsDTO.news.id}">
                    <div class="row">
                        <div class="col-sm-4">
                            <p>${comment.creationDate}</p>
                            <div class="bg-info">${comment.text}
                                <button type="submit" class="btn btn-link"><span class="glyphicon glyphicon-remove"></span></button>
                            </div>
                        </div>
                    </div>
                </form>
            </c:forEach>

            <form method="POST" action="/news-admin/addComment">
                <input type="hidden" name="newsId" value="${newsDTO.news.id}">
                <div class="form-group">

                    <textarea class="form-control" rows="5" maxlength="20" name="commentText"></textarea>
                </div>
                <input type="submit" class="btn btn-primary" value="<spring:message code="addComment"/>"/>
            </form>

        </div>
    </div>
</div>