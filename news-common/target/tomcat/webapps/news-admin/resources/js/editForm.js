 function edit(id) {
        if (document.getElementById(id).readOnly == false) {
            document.getElementById(id).readOnly = true;
                $("#edit"+id).show();
                $("#update"+id).hide();
                $("#expire"+id).hide();
                $("#cancel"+id).hide();
        }
        else {
                $("#edit"+id).hide();
                $("#update"+id).show();
                $("#expire"+id).show();
                $("#cancel"+id).show();
            document.getElementById(id).readOnly = false;
        }
}