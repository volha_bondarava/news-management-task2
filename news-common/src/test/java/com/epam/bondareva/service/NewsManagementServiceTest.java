package com.epam.bondareva.service;

import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.impl.NewsManagementService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;

import static org.mockito.Mockito.*;

/**
* Created by Olya Bondareva on 25/03/2015.
*/
@ContextConfiguration(locations = {"classpath:spring-config.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class NewsManagementServiceTest {
    @InjectMocks
    private NewsManagementService newsManagementService;
    @Mock
    private INewsService newsService;
    @Mock
    private ICommentService commentService;
    @Mock
    private ITagService tagService;
    @Mock
    private IAuthorService authorService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addNewsDTO() throws ServiceException {
        NewsDTO newsDTO = new NewsDTO();
        News news = new News();
        news.setId(2L);
        newsDTO.setNews(news);
        Author author = new Author();
        author.setId(2L);
        newsDTO.setAuthor(author);

        newsManagementService.addNewsDTO(newsDTO);

        verify(newsService, times(1)).addNews(newsDTO.getNews());
        verify(authorService, times(1)).addAuthorNews(newsDTO.getAuthor().getId(), newsDTO.getNews().getId());
        verify(tagService, times(1)).addNewsTagList(newsDTO.getNews().getId(), newsDTO.getTagList());
        verifyNoMoreInteractions(tagService, newsService, authorService);
    }

    @Test
    public void getPrevNewsDTO() throws Exception{
        SearchCriteria searchCriteria = new SearchCriteria();
        int place = 2;
        News news = new News();
        news.setId(2L);

        when(newsService.viewCurrentNews(searchCriteria, place)).thenReturn(news);
        newsManagementService.getCurrentNewsDTO(searchCriteria, place);

        verify(newsService, times(1)).viewCurrentNews(searchCriteria, place);
        verify(authorService, times(1)).getNewsAuthor(news.getId());
        verify(tagService, times(1)).getTagListByNewsId(news.getId());
        verify(commentService, times(1)).getCommentListByNewsId(news.getId());
        verifyNoMoreInteractions(authorService, newsService, commentService, tagService);
    }

    @Test
    public void deleteNews() throws Exception {
        Long newsId = 2L;

        newsManagementService.deleteNews(newsId);
        verify(commentService, times(1)).deleteNewsComments(newsId);
        verify(newsService, times(1)).deleteNews(newsId);
        verify(tagService, times(1)).deleteNewsTag(newsId);
        verify(authorService, times(1)).deleteAuthorNews(newsId);

        verifyNoMoreInteractions(authorService, newsService, commentService, tagService);
    }

}