package com.epam.bondareva.service;

import com.epam.newsmanagement.dao.IAuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.AuthorService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Olya Bondareva on 03/04/2015.
 */
@ContextConfiguration(locations = {"classpath:spring-config.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class AuthorServiceTest {
    @Mock
    private IAuthorDao authorDao;
    @InjectMocks
    private AuthorService authorService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addAuthor() throws Exception {
        Author author = new Author();
        author.setId(2L);
        author.setName("author1");

        when(authorDao.create(author)).thenReturn(author.getId());
        assertEquals((long) author.getId(), authorService.addAuthor(author));
        verify(authorDao, times(1)).create(author);
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void addAuthorNews() throws Exception {
        Long authorId = 2L;
        Long newsId = 3L;
        authorService.addAuthorNews(authorId, newsId);
        verify(authorDao, times(1)).addAuthorNews(authorId, newsId);
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void deleteAuthorNews() throws Exception {
        Long newsId = 3L;
        authorService.deleteAuthorNews(newsId);
        verify(authorDao, times(1)).deleteAuthorNews(newsId);
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void deleteAuthor() throws Exception {
        Long authorId = 3L;
        authorService.deleteAuthor(authorId);
        verify(authorDao, times(1)).delete(authorId);
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void getNewsAuthor() throws Exception {
        Long newsId = 3L;
        authorService.getNewsAuthor(newsId);
        verify(authorDao, times(1)).getNewsAuthor(newsId);
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void getAuthorList() throws Exception {
        authorService.getAuthorList();
        verify(authorDao, times(1)).getList();
        verifyNoMoreInteractions(authorDao);
    }

    @Test
    public void expireAuthor() throws Exception {
        Long authorId = 3L;
        Date expiredDate = new Date();
        authorService.expireAuthor(authorId, expiredDate);
        verify(authorDao, times(1)).expireAuthor(authorId, expiredDate);
        verifyNoMoreInteractions(authorDao);
    }

    public void updateAuthor() throws Exception {
        Author author = new Author();
        author.setId(3L);
        author.setName("author1");
        author.setExpire(new Date());
        verify(authorDao, times(1)).update(author);
        verifyNoMoreInteractions(authorDao);
    }

    public void getAuthorById() throws Exception {
        Long authorId = 3L;
        verify(authorDao, times(1)).getById(authorId);
        verifyNoMoreInteractions(authorDao);
    }
}
