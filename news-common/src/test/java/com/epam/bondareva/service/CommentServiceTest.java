package com.epam.bondareva.service;

import com.epam.newsmanagement.dao.ICommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.CommentService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Olya Bondareva on 03/04/2015.
 */
@ContextConfiguration(locations = {"classpath:spring-config.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class CommentServiceTest {
    @Mock
    private ICommentDao commentDao;
    @InjectMocks
    private CommentService commentService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addComment() throws Exception {
        Comment comment = new Comment();
        comment.setId(1L);
        comment.setText("comment1");
        comment.setCreationDate(new Timestamp(0));
        comment.setNewsId(1L);

        when(commentDao.create(comment)).thenReturn(comment.getId());
        assertEquals((long)comment.getId(), (long)commentService.addComment(comment));
        verify(commentDao, times(1)).create(comment);
        verifyNoMoreInteractions(commentDao);
    }

    @Test(expected=ServiceException.class)
    public void commentWasNotDeleted() throws Exception {
        Long commentId = 0L;
        when(commentDao.getById(commentId)).thenReturn(null);
        commentService.deleteComment(commentId);
        verify(commentDao, times(0)).delete(commentId);
        verify(commentDao, times(1)).getById(commentId);
        verifyNoMoreInteractions(commentDao);
    }

    @Test
    public void commentWasDeleted() throws Exception {
        Long commentId = 1L;

        Comment comment = new Comment();
        comment.setId(1L);
        comment.setText("comment1");
        comment.setCreationDate(new Timestamp(0));
        comment.setNewsId(1L);

        when(commentDao.getById(commentId)).thenReturn(comment);
        commentService.deleteComment(commentId);
        verify(commentDao, times(1)).delete(commentId);
        verify(commentDao, times(1)).getById(commentId);
        verifyNoMoreInteractions(commentDao);
    }
}
