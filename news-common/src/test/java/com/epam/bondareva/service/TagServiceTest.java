package com.epam.bondareva.service;

import com.epam.newsmanagement.dao.ITagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.TagService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Olya Bondareva on 03/04/2015.
 */
@ContextConfiguration(locations = {"classpath:spring-config.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TagServiceTest {
    @Mock
    private ITagDao tagDao;
    @InjectMocks
    private TagService tagService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addTag() throws Exception {
        Tag tag = new Tag();
        tag.setId(2L);
        tag.setName("tag1");

        when(tagDao.create(tag)).thenReturn(tag.getId());
        assertEquals((long)tag.getId(), (long)tagService.addTag(tag));
        verify(tagDao, times(1)).create(tag);
        verifyNoMoreInteractions(tagDao);
    }

    @Test(expected=ServiceException.class)
    public void tagWasNotDeletedTag() throws Exception {
        Long tagId = 2L;
        when(tagDao.getById(tagId)).thenReturn(null);
        tagService.deleteTag(tagId);
        verify(tagDao, times(0)).delete(tagId);
        verify(tagDao, times(1)).getById(tagId);
        verifyNoMoreInteractions(tagDao);
    }

    @Test
    public void newsWasDeleted() throws Exception {
        Tag tag = new Tag();
        tag.setId(1L);
        tag.setName("tag1");
        Long tagId = 1L;
        when(tagDao.getById(tagId)).thenReturn(tag);
        tagService.deleteTag(tagId);
        verify(tagDao, times(1)).delete(tagId);
        verify(tagDao, times(1)).deleteFromAllNews(tag.getId());
        verify(tagDao, times(1)).getById(tagId);
        verifyNoMoreInteractions(tagDao);
    }
}
