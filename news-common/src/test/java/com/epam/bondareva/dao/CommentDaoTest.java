package com.epam.bondareva.dao;

import com.epam.newsmanagement.dao.ICommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;

/**
* Created by Olya Bondareva on 07/04/2015.
*/

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring-config.xml"})
@TestExecutionListeners(listeners = { DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class ,
        DbUnitTestExecutionListener.class})
@DatabaseSetup(value = "classpath:sample/sample.xml")
public class CommentDaoTest {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private ICommentDao commentDao;

    @Test
    public void create() throws DaoException {
        Comment comment = new Comment();
        comment.setId(11L);
        comment.setText("comment11");
        comment.setNewsId(1L);
        comment.setCreationDate(new Date(0));
        Long commentId = commentDao.create(comment);
        assertThat(commentId, greaterThan(0L));
    }

    @Test
    @ExpectedDatabase(value = "classpath:expected/deleteComment.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void delete() throws DaoException {
        Long commentId = 2L;
        commentDao.delete(commentId);
    }

    @Test
    @ExpectedDatabase(value = "classpath:expected/updateComment.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void update() throws DaoException {
        Comment comment = new Comment();
        comment.setText("commentText22");
        comment.setCreationDate(new Date(0));
        comment.setNewsId(2L);
        comment.setId(2L);
        commentDao.update(comment);
    }

    @Test
    public void getEntityById() throws DaoException {
        Long commentId = 2L;
        Comment comment = new Comment();
        comment.setText("commentText2");
        comment.setNewsId(2L);
        comment.setCreationDate(new Date(0));
        comment.setId(commentId);
        assertEquals(comment, commentDao.getById(commentId));
    }

    @Test
    public void getEntityListByNewsId() throws DaoException {
        List<Comment> commentList = new ArrayList<Comment>();
        Long newsId = 2L;
        Comment comment = new Comment();
        comment.setId(2L);
        comment.setText("commentText2");
        comment.setNewsId(newsId);
        comment.setCreationDate(new Date(0));
        commentList.add(comment);

        assertTrue(commentList.containsAll(commentDao.getEntityListByNewsId(newsId)));
        assertEquals(commentList.size(), commentDao.getEntityListByNewsId(newsId).size());
    }
}
