package com.epam.bondareva.dao;

import com.epam.newsmanagement.dao.IAuthorDao;
import com.epam.newsmanagement.dao.ICommentDao;
import com.epam.newsmanagement.dao.INewsDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;

/**
* Created by Olya Bondareva on 07/04/2015.
*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring-config.xml"})
@TestExecutionListeners(listeners = { DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class ,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:sample/sample.xml")
public class NewsDaoTest {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private INewsDao newsDao;
    @Autowired
    private ICommentDao commentDao;
    @Autowired
    private IAuthorDao authorDao;

    @Test
    public void create() throws DaoException {
        News news = new News();
        news.setId(11L);
        news.setTitle("title11");
        news.setShortText("shortText11");
        news.setFullText("fullText11");
        news.setModificationDate(new Date(0));
        news.setCreationDate(new Date(0));
        Long newsId = newsDao.create(news);
        assertThat(newsId, greaterThan(0L));
    }

    @Test
    public void getEntityById() throws DaoException {
        Long newsId = 2L;
        News news = new News();
        news.setTitle("title2");
        news.setShortText("shortText2");
        news.setModificationDate(new Date(0));
        news.setCreationDate(new Date(0));
        news.setFullText("fullText2");
        news.setId(newsId);
        assertEquals(news, newsDao.getById(newsId));
    }
    @Test
    public void searchByTags() throws DaoException{
        int start = 0;
        int end = 10;
        SearchCriteria searchCriteria = new SearchCriteria();
        List<News> newsList = new ArrayList<>();
        int amount = 10;
        News news;
        for(int i = 1; i <= amount; i++) {
            news = new News();
            news.setTitle("title"+i);
            news.setShortText("shortText"+i);
            news.setModificationDate(new Date(0));
            news.setCreationDate(new Date(0));
            news.setFullText("fullText"+i);
            news.setId((long)i);
            newsList.add(news);
        }
        List<Long> tagIdList = new ArrayList<>();
        for(int i = 1; i <= amount; i++) {
            tagIdList.add((long)i);
        }
        searchCriteria.setTagsId(tagIdList);
        assertTrue(newsList.containsAll(newsDao.searchByAuthorAndTag(start, end, searchCriteria)));
        assertEquals(newsList.size(), newsDao.searchByAuthorAndTag(start, end, searchCriteria).size());
    }

    @Test
    public void searchByAuthorAndTag() throws DaoException{
        int start = 0;
        int end = 3;
        SearchCriteria searchCriteria = new SearchCriteria();
        Long authorId = 2L;
        searchCriteria.setAuthorId(authorId);
        List<News> newsList = new ArrayList<>();
        News news = new News();
        news.setTitle("title2");
        news.setShortText("shortText2");
        news.setModificationDate(new Date(0));
        news.setCreationDate(new Date(0));
        news.setFullText("fullText2");
        news.setId(2L);
        newsList.add(news);

        assertTrue(newsList.containsAll(newsDao.searchByAuthorAndTag(start, end, searchCriteria)));
        assertEquals(newsList.size(), newsDao.searchByAuthorAndTag(start, end, searchCriteria).size());
    }

    @Test
    public void getPrevNews() throws DaoException{
        int position = 3;
        SearchCriteria searchCriteria = new SearchCriteria();
        News news = new News();
        news.setTitle("title4");
        news.setShortText("shortText4");
        news.setModificationDate(new Date(0));
        news.setCreationDate(new Date(0));
        news.setFullText("fullText4");
        news.setId(4L);
        assertEquals(news, newsDao.getCurrentNews(searchCriteria, position));
    }

    @Test
    public void searchByTag() throws DaoException {
        Tag tag = new Tag();
        tag.setId(2L);
        tag.setName("tagName2");

        News news = new News();
        news.setTitle("title2");
        news.setShortText("shortText2");
        news.setModificationDate(new Date(0));
        news.setCreationDate(new Date(0));
        news.setFullText("fullText2");
        news.setId(2L);
        List<News> newsList = new ArrayList<>();
        newsList.add(news);

        assertTrue(newsList.containsAll(newsDao.searchByTag(tag)));
        assertEquals(newsList.size(), newsDao.searchByTag(tag).size());
    }

    @Test
    public void searchByAuthor() throws DaoException {
        Author author = new Author();
        author.setId(2L);
        author.setName("author2");

        News news = new News();
        news.setTitle("title2");
        news.setShortText("shortText2");
        news.setModificationDate(new Date(0));
        news.setCreationDate(new Date(0));
        news.setFullText("fullText2");
        news.setId(2L);
        List<News> newsList = new ArrayList<>();
        newsList.add(news);

        assertTrue(newsList.containsAll(newsDao.searchByAuthor(author)));
        assertEquals(newsList.size(), newsDao.searchByAuthor(author).size());
    }
}
