package com.epam.bondareva.dao;

import com.epam.newsmanagement.dao.ITagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;

/**
* Created by Olya Bondareva on 07/04/2015.
*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:spring-config.xml"})
@TestExecutionListeners(listeners = { DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class ,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:sample/sample.xml")
public class TagDaoTest {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private ITagDao tagDao;

    @Test
    @ExpectedDatabase(value = "classpath:expected/createTag.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void create() throws DaoException {
        Tag tag = new Tag();
        tag.setName("tagName11");
        Long tagId = tagDao.create(tag);
        assertThat(tagId, greaterThan(0L));
    }

    @Test
    @ExpectedDatabase(value = "classpath:expected/deleteTag.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void delete() throws DaoException {
        Long tagId = 2L;
        tagDao.deleteFromAllNews(tagId);
        tagDao.delete(tagId);
    }

    @Test
    @ExpectedDatabase(value = "classpath:expected/updateTag.xml",assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void update() throws DaoException {
        Tag tag = new Tag();
        tag.setId(2L);
        tag.setName("tagName22");
        tagDao.update(tag);
    }

    @Test
    public void getEntityById() throws DaoException {
        Long tagId = 2L;
        Tag tag = new Tag();
        tag.setId(tagId);
        tag.setName("tagName2");
        assertEquals(tag, tagDao.getById(tagId));
    }

    @Test
    public void getEntityList() throws DaoException {
        List<Tag> tagList = new ArrayList<>();
        int amount = 10;
        for(int i= 1; i <= amount; i++) {
            Tag tag = new Tag();
            tag.setId((long)i);
            tag.setName("tagName"+i);
            tagList.add(tag);
        }
        assertTrue(tagList.containsAll(tagDao.getList()));
        assertEquals(tagList.size(), tagDao.getList().size());
    }

    @Test
    public void getEntityListByNewsId() throws DaoException {
        List<Tag> tagList = new ArrayList<>();
        Long newsId = 2L;
            Tag tag = new Tag();
            tag.setId(2L);
            tag.setName("tagName2");
            tagList.add(tag);
        assertTrue(tagList.containsAll(tagDao.getEntityListByNewsId(newsId)));
        assertEquals(tagList.size(), tagDao.getEntityListByNewsId(newsId).size());
    }
}
