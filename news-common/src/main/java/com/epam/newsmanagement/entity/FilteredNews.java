package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.List;

public class FilteredNews implements Serializable{
    List<NewsDTO> newsDTOList;
    int matchedCount;

    public FilteredNews() {
    }

    public List<NewsDTO> getNewsDTOList() {
        return newsDTOList;
    }

    public void setNewsDTOList(List<NewsDTO> newsDTOList) {
        this.newsDTOList = newsDTOList;
    }

    public int getMatchedCount() {
        return matchedCount;
    }

    public void setMatchedCount(int matchedCount) {
        this.matchedCount = matchedCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if((o == null) || (o.getClass() != this.getClass())) return false;

        FilteredNews that = (FilteredNews) o;

        if (getMatchedCount() != that.getMatchedCount()) return false;
        return !(getNewsDTOList() != null ? !getNewsDTOList().equals(that.getNewsDTOList()) : that.getNewsDTOList() != null);

    }

    @Override
    public int hashCode() {
        int result = getNewsDTOList() != null ? getNewsDTOList().hashCode() : 0;
        result = 31 * result + getMatchedCount();
        return result;
    }

    @Override
    public String toString() {
        return "FilteredNews{" +
                "newsDTOList=" + newsDTOList +
                ", matchedCount=" + matchedCount +
                '}';
    }
}
