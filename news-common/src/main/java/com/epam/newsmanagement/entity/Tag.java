package com.epam.newsmanagement.entity;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Created by Olya Bondareva on 15/03/2015.
 */
public class Tag extends Entity implements Serializable{
    private static final long serialVersionUID = 803659337947943050L;
    @Size(min=3, max=30,
            message="Tag name must be between 3 and 20 characters long.")
    private String name;

    public Tag(){
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tag)) return false;
        if (!super.equals(o)) return false;

        Tag tag = (Tag) o;

        return !(getName() != null ? !getName().equals(tag.getName()) : tag.getName() != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("name=").append(name);
        return sb.toString();
    }
}
