package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Olya Bondareva on 14/04/2015.
 */

public class NewsDTO implements Serializable{
    private static final long serialVersionUID = -6708273945351161497L;
    private News news;
    private Author author;
    private List<Tag> tagList;
    private List<Comment> commentList;

    public NewsDTO() {
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if((o == null) || (o.getClass() != this.getClass())) return false;

        NewsDTO newsDTO = (NewsDTO) o;

        if (!getNews().equals(newsDTO.getNews())) return false;
        if (!getAuthor().equals(newsDTO.getAuthor())) return false;
        if (getTagList() != null ? !getTagList().equals(newsDTO.getTagList()) : newsDTO.getTagList() != null)
            return false;
        return !(getCommentList() != null ? !getCommentList().equals(newsDTO.getCommentList()) : newsDTO.getCommentList() != null);

    }

    @Override
    public int hashCode() {
        int result = getNews().hashCode();
        result = 31 * result + getAuthor().hashCode();
        result = 31 * result + (getTagList() != null ? getTagList().hashCode() : 0);
        result = 31 * result + (getCommentList() != null ? getCommentList().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("news=").append(news);
        sb.append("author=").append(author);
        sb.append("tagList=").append(tagList);
        sb.append("commentList=").append(commentList);
        return sb.toString();
    }
}
