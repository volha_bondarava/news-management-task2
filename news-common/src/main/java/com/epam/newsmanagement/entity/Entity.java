package com.epam.newsmanagement.entity;

import java.io.Serializable;

/**
 * Created by Olya Bondareva on 15/03/2015.
 */
public abstract class Entity implements Serializable{
    private static final long serialVersionUID = 4018135018505689380L;

    private Long id;

    public Entity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("id=").append(id);
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Entity)) return false;

        Entity entity = (Entity) o;

        return !(getId() != null ? !getId().equals(entity.getId()) : entity.getId() != null);

    }

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }
}