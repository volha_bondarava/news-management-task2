package com.epam.newsmanagement.entity;

import java.util.List;

public class SearchCriteria {
    private Long authorId;
    private List<Long> tagsId;

    public SearchCriteria() {
    }

    public List<Long> getTagsId() {
        return tagsId;
    }

    public void setTagsId(List<Long> tagsId) {
        this.tagsId = tagsId;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if((o == null) || (o.getClass() != this.getClass())) return false;

        SearchCriteria that = (SearchCriteria) o;

        if (!getAuthorId().equals(that.getAuthorId())) return false;
        return !(getTagsId() != null ? !getTagsId().equals(that.getTagsId()) : that.getTagsId() != null);

    }

    @Override
    public int hashCode() {
        int result = getAuthorId().hashCode();
        result = 31 * result + (getTagsId() != null ? getTagsId().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FilteredNews{" +
                "authorId=" + authorId +
                ", tagsId=" + tagsId +
                '}';
    }
}
