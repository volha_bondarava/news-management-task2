package com.epam.newsmanagement.entity;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Olya Bondareva on 15/03/2015.
 */
public class Author extends Entity implements Serializable{

    private static final long serialVersionUID = 929619449984602161L;
    @Size(min=3, max=30,
            message="Author name must be between 3 and 20 characters long.")
    private String name;
    private Date expire;

    public Author() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getExpire() {
        return expire;
    }

    public void setExpire(Date expire) {
        this.expire = expire;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Author)) return false;
        if (!super.equals(o)) return false;

        Author author = (Author) o;

        if (getName() != null ? !getName().equals(author.getName()) : author.getName() != null) return false;
        return !(getExpire() != null ? !getExpire().equals(author.getExpire()) : author.getExpire() != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getExpire() != null ? getExpire().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("name=").append(name);
        sb.append("expire=").append(expire);
        return sb.toString();
    }
}
