package com.epam.newsmanagement.entity;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Olya Bondareva on 15/03/2015.
 */
public class Comment extends Entity implements Serializable{
    private static final long serialVersionUID = -5723781317753794521L;
    @Size(min=3, max=30,
            message="Comment text must be between 3 and 20 characters long.")
    private String text;
    private Date creationDate;
    private Long newsId;

    public Comment() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setNewsId(Long newsId) {
        this.newsId = newsId;
    }

    public Long getNewsId() {
        return newsId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Comment)) return false;
        if (!super.equals(o)) return false;

        Comment comment = (Comment) o;

        if (getText() != null ? !getText().equals(comment.getText()) : comment.getText() != null) return false;
        if (getCreationDate() != null ? !getCreationDate().equals(comment.getCreationDate()) : comment.getCreationDate() != null)
            return false;
        return !(getNewsId() != null ? !getNewsId().equals(comment.getNewsId()) : comment.getNewsId() != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getText() != null ? getText().hashCode() : 0);
        result = 31 * result + (getCreationDate() != null ? getCreationDate().hashCode() : 0);
        result = 31 * result + (getNewsId() != null ? getNewsId().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("message=").append(text);
        sb.append("creationDate=").append(creationDate);
        sb.append("newsId=").append(newsId);
        return sb.toString();
    }
}
