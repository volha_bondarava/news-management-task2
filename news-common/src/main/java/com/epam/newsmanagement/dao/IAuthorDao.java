package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;

import java.util.Date;

/**
 * Created by Olya Bondareva on 29/03/2015.
 */
public interface IAuthorDao extends IDao<Author> {
    void addAuthorNews(Long authorId, Long newsId) throws DaoException;
    void deleteAuthorNews(Long newsId) throws DaoException;
    Author getNewsAuthor(Long newsId) throws DaoException;
    void expireAuthor(Long authorId, Date expiredDate) throws DaoException;
}
