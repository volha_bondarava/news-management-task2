package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.ICommentDao;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.entity.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
@Repository
public class CommentDao implements ICommentDao {
    private String DELETE_COMMENT = "DELETE FROM COMMENTS WHERE CO_COMMENT_ID_PK = ?";
    private String SELECT_COMMENTS_BY_NEWS_ID = "SELECT CO_COMMENT_ID_PK, CO_COMMENT_TEXT, CO_CREATION_DATE, CO_NEWS_ID_FK FROM COMMENTS WHERE CO_NEWS_ID_FK = ?";
    private String SELECT_COMMENT_BY_ID = "SELECT CO_COMMENT_ID_PK, CO_COMMENT_TEXT, CO_CREATION_DATE, CO_NEWS_ID_FK FROM COMMENTS WHERE CO_COMMENT_ID_PK = ?";
    private String UPDATE_COMMENT = "UPDATE COMMENTS SET CO_COMMENT_TEXT = ? WHERE CO_COMMENT_ID_PK = ?";
    private String INSERT_INTO_COMMENTS = "INSERT INTO COMMENTS (CO_COMMENT_ID_PK, CO_COMMENT_TEXT,CO_CREATION_DATE,CO_NEWS_ID_FK) VALUES(SEQ_COMMENT.NEXTVAL, ?,?,?)";
    private String DELETE_NEWS_FROM_COMMENTS = "DELETE FROM COMMENTS WHERE CO_NEWS_ID_FK=? AND CO_COMMENT_ID_PK=?";
    private String SELECT_COMMENT_LIST = "SELECT CO_COMMENT_ID_PK, CO_COMMENT_TEXT, CO_CREATION_DATE, CO_NEWS_ID_FK FROM COMMENTS";
    private String DELETE_COMMENTS = "DELETE FROM COMMENTS WHERE CO_NEWS_ID_FK=?";
    @Autowired
    private DataSource dataSource;

    public CommentDao() {
    }

    @Override
    public Long create(Comment comment) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        Long commentId = null;
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(INSERT_INTO_COMMENTS, new String[]{"CO_COMMENT_ID_PK"})
        ) {
            preparedStatement.setString(1, comment.getText());
            preparedStatement.setTimestamp(2, new Timestamp(comment.getCreationDate().getTime()));
            preparedStatement.setLong(3, comment.getNewsId());
            if (preparedStatement.executeUpdate() > 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys != null && generatedKeys.next()) {
                        commentId = generatedKeys.getLong(1);
                    }
                } catch (SQLException e) {
                    throw new DaoException(e);
                }
            }
            return commentId;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void update(Comment comment) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(UPDATE_COMMENT)
        ) {
            preparedStatement.setString(1, comment.getText());
            preparedStatement.setLong(2, comment.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void delete(Long commentId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(DELETE_COMMENT)
        ) {
            preparedStatement.setLong(1, commentId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Comment getById(Long commentId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_COMMENT_BY_ID)
        ) {
            preparedStatement.setLong(1, commentId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                Comment comment = null;
                if (resultSet.next()) {
                    comment = new Comment();
                    comment.setId(resultSet.getLong(1));
                    comment.setText(resultSet.getString(2));
                    comment.setCreationDate(resultSet.getDate(3));
                    comment.setNewsId(resultSet.getLong(4));
                }
                return comment;
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Comment> getList() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement1 =
                     connection.prepareStatement(SELECT_COMMENT_LIST)
        ) {
            try (ResultSet resultSet = preparedStatement1.executeQuery()) {
                List<Comment> commentList = new ArrayList<>();
                while (resultSet.next()) {
                    Comment comment = new Comment();
                    comment.setId(resultSet.getLong(1));
                    comment.setText(resultSet.getString(2));
                    comment.setCreationDate(resultSet.getDate(3));
                    commentList.add(comment);
                }
                return commentList;
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<Comment> getEntityListByNewsId(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement1 =
                     connection.prepareStatement(SELECT_COMMENTS_BY_NEWS_ID)
        ) {
            preparedStatement1.setLong(1, newsId);
            try (ResultSet resultSet = preparedStatement1.executeQuery()) {
                List<Comment> commentList = new ArrayList<>();
                while (resultSet.next()) {
                    Comment comment = new Comment();
                    comment.setId(resultSet.getLong(1));
                    comment.setText(resultSet.getString(2));
                    comment.setCreationDate(resultSet.getDate(3));
                    comment.setNewsId(resultSet.getLong(4));
                    commentList.add(comment);
                }
                return commentList;
            } catch (SQLException e) {
                throw new DaoException(e);
            }

        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteNewsComment(Long newsId, Long commentId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(DELETE_NEWS_FROM_COMMENTS)
        ) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.setLong(2, commentId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteNewsComments(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(DELETE_COMMENTS)
        ) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
}
