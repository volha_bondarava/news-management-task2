package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;

import java.util.List;

/**
 * Created by Olya Bondareva on 29/03/2015.
 */
public interface ITagDao extends IDao<Tag> {
    List<Tag> getEntityListByNewsId(Long newsId) throws DaoException;
    void deleteFromAllNews(Long tagId) throws DaoException;
    void addNewsTagList(Long newsId, List<Tag> tagList) throws DaoException;
    void deleteTags(Long newsId) throws  DaoException;
    void addNewsTag(Long newsId, Long tagId) throws DaoException;
    void deleteNewsTag(Long newsId, Long tagId) throws DaoException;
}
