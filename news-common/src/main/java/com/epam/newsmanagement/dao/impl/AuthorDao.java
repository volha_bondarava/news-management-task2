package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.IAuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
@Repository
public class AuthorDao implements IAuthorDao {
    private String GET_AUTHOR_BY_ID = "SELECT AU_AUTHOR_ID_PK, AU_NAME,AU_EXPIRE FROM AUTHORS WHERE AU_AUTHOR_ID_PK = ?";
    private String INSERT_INTO_AUTHORS = "INSERT INTO AUTHORS (AU_AUTHOR_ID_PK,AU_NAME) VALUES (SEQ_AUTHOR.NEXTVAL,?)";
    private String UPDATE_AUTHOR = "UPDATE AUTHORS SET AU_NAME = ? WHERE AU_AUTHOR_ID_PK = ?";
    private String DELETE_AUTHOR = "DELETE FROM AUTHORS WHERE AU_AUTHOR_ID_PK = ?";
    private String GET_AUTHOR_LIST = "SELECT AU_AUTHOR_ID_PK, AU_NAME,AU_EXPIRE FROM AUTHORS";
    private String INSERT_INTO_NEWS_AUTHORS = "INSERT INTO NEWS_AUTHORS(NA_NEWS_AUTHOR_ID_PK,NA_AUTHOR_ID_FK,NA_NEWS_ID_FK) VALUES(SEQ_NEWS_AUTHOR.NEXTVAL,?,?)";
    private String DELETE_FROM_NEWS_AUTHORS = "DELETE FROM NEWS_AUTHORS WHERE NA_NEWS_ID_FK=?";
    private String GET_NEWS_AUTHOR = "SELECT AU_AUTHOR_ID_PK, AU_NAME,AU_EXPIRE FROM AUTHORS JOIN NEWS_AUTHORS ON AUTHORS.AU_AUTHOR_ID_PK=NEWS_AUTHORS.NA_AUTHOR_ID_FK WHERE NA_NEWS_ID_FK = ?";
    private String EXPIRE_AUTHOR = "UPDATE AUTHORS SET AU_EXPIRE=? WHERE AU_AUTHOR_ID_PK=?";
    @Autowired
    private DataSource dataSource;

    public AuthorDao() {
    }

    public void setDataSource(DataSource dataSource) throws DaoException {
        this.dataSource = dataSource;
    }

    @Override
    public Long create(Author author) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(INSERT_INTO_AUTHORS, new String[]{"AU_AUTHOR_ID_PK"})
        ) {
            Long authorId = null;

            preparedStatement.setString(1, author.getName());
            if (preparedStatement.executeUpdate() > 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys != null && generatedKeys.next()) {
                        authorId = generatedKeys.getLong(1);
                    }
                } catch (SQLException e) {
                    throw new DaoException(e);
                }
            }
            return authorId;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void update(Author author) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(UPDATE_AUTHOR)
        ) {
            preparedStatement.setString(1, author.getName());
            preparedStatement.setLong(2, author.getId());
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void delete(Long authorId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(DELETE_AUTHOR)
        ) {
            preparedStatement.setLong(1, authorId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Author getById(Long authorId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(GET_AUTHOR_BY_ID)
        ) {
            preparedStatement.setLong(1, authorId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                Author author = null;
                if (resultSet.next()) {
                    author = new Author();
                    author.setId(resultSet.getLong(1));
                    author.setName(resultSet.getString(2));
                    author.setExpire(resultSet.getDate(3));
                }
                return author;
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Author> getList() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (Statement statement =
                     connection.createStatement()
        ) {
            try (ResultSet resultSet = statement.executeQuery(GET_AUTHOR_LIST)
            ) {
                List<Author> authorList = new ArrayList<>();
                Author author;
                while (resultSet.next()) {
                    author = new Author();
                    author.setId(resultSet.getLong(1));
                    author.setName(resultSet.getString(2));
                    author.setExpire(resultSet.getDate(3));
                    authorList.add(author);
                }
                return authorList;
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void addAuthorNews(Long authorId, Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(INSERT_INTO_NEWS_AUTHORS)
        ) {
            preparedStatement.setLong(1, authorId);
            preparedStatement.setLong(2, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteAuthorNews(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(DELETE_FROM_NEWS_AUTHORS)
        ) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Author getNewsAuthor(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(GET_NEWS_AUTHOR)
        ) {
            preparedStatement.setLong(1, newsId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                Author author = null;
                if (resultSet.next()) {
                    author = new Author();
                    author.setId(resultSet.getLong(1));
                    author.setName(resultSet.getString(2));
                    author.setExpire(resultSet.getDate(3));
                }
                return author;
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void expireAuthor(Long authorId, Date expiredDate) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(EXPIRE_AUTHOR)
        ) {
            preparedStatement.setTimestamp(1, new Timestamp(expiredDate.getTime()));
            preparedStatement.setLong(2, authorId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


}
