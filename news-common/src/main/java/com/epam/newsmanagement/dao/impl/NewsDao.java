package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.INewsDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
@Repository
//WITH NUMBERED_QUERY AS(SELECT NEWS.*, ROWNUM AS NE_NEWS_ID_PK FROM(
//        SELECT NE_NEWS_ID_PK,NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE FROM NEWS LEFT JOIN COMMENTS ON NEWS.NE_NEWS_ID_PK=COMMENTS.CO_NEWS_ID_FK GROUP BY NE_NEWS_ID_PK,NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE ORDER BY COUNT(CO_NEWS_ID_FK) DESC,NE_CREATION_DATE ASC)
//        NEWS)
//        SELECT NEWS.* FROM NUMBERED_QUERY NEWS, (SELECT MIN(NEWS.NE_NEWS_ID_PK) AS NE_NEWS_ID_PK FROM NEWS WHERE NE_NEWS_ID_PK = 2) NE_NEWS_ID_PK WHERE NE_NEWS_ID_PK IN (NEWS.NE_NEWS_ID_PK-1,NEWS.NE_NEWS_ID_PK+1);
public class NewsDao implements INewsDao {
    private String INSERT_INTO_NEWS = "INSERT INTO NEWS(NE_NEWS_ID_PK,NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE) VALUES (SEQ_NEWS.NEXTVAL,?,?,?,?,?)";
    private String DELETE_NEWS = "DELETE FROM NEWS WHERE NE_NEWS_ID_PK = ?";
    private String SELECT_NEWS_BY_ID = "SELECT NE_NEWS_ID_PK, NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE FROM NEWS WHERE NE_NEWS_ID_PK = ?";
    private String SELECT_IN_ORDER_OF_MOST_COMMENTED_NEWS = "SELECT NE_NEWS_ID_PK,NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE\n" +
            "FROM NEWS LEFT JOIN COMMENTS ON NEWS.NE_NEWS_ID_PK=COMMENTS.CO_NEWS_ID_FK\n" +
            "GROUP BY NE_NEWS_ID_PK,NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE ORDER BY COUNT(CO_NEWS_ID_FK) DESC, NE_CREATION_DATE ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
    private String SELECT_NEWS_BY_TAG = "SELECT NE_NEWS_ID_PK,NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE \n" +
            "FROM NEWS INNER JOIN NEWS_TAGS ON NEWS.NE_NEWS_ID_PK = NEWS_TAGS.NT_NEWS_ID_FK WHERE NT_TAG_ID_FK=?";
    private String SELECT_NEWS_BY_AUTHOR = "SELECT NE_NEWS_ID_PK,NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE \n" +
            "FROM NEWS INNER JOIN NEWS_AUTHORS ON NEWS.NE_NEWS_ID_PK = NEWS_AUTHORS.NA_NEWS_ID_FK WHERE NA_AUTHOR_ID_FK=?";
    private String UPDATE_NEWS = "UPDATE NEWS SET NE_FULL_TEXT=?,NE_MODIFICATION_DATE=?,NE_SHORT_TEXT=?,NE_TITLE =? WHERE NE_NEWS_ID_PK=?";
    private String COUNT_ALL_NEWS = "SELECT COUNT(NE_NEWS_ID_PK) FROM NEWS";
    @Autowired
    private DataSource dataSource;

    public NewsDao() {
    }

    @Override
    public Long create(News news) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);

        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(INSERT_INTO_NEWS, new String[]{"NE_NEWS_ID_PK"});
        ) {
            Long newsId = null;

            preparedStatement.setString(1, news.getFullText());
            preparedStatement.setTimestamp(2, new Timestamp(news.getCreationDate().getTime()));
            preparedStatement.setTimestamp(3, new Timestamp(news.getModificationDate().getTime()));
            preparedStatement.setString(4, news.getShortText());
            preparedStatement.setString(5, news.getTitle());

            if (preparedStatement.executeUpdate() > 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys != null && generatedKeys.next()) {
                        newsId = generatedKeys.getLong(1);
                    }
                } catch (SQLException e) {
                    throw new DaoException(e);
                }
            }
            return newsId;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


    @Override
    public void update(News news) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_NEWS)
        ) {
            preparedStatement.setString(1, news.getFullText());
            preparedStatement.setTimestamp(2, new Timestamp(news.getModificationDate().getTime()));
            preparedStatement.setString(3, news.getShortText());
            preparedStatement.setString(4, news.getTitle());
            preparedStatement.setLong(5, news.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void delete(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement1 =
                     connection.prepareStatement(DELETE_NEWS)
        ) {
            preparedStatement1.setLong(1, newsId);
            preparedStatement1.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public News getById(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_NEWS_BY_ID)) {
            preparedStatement.setLong(1, newsId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                News news = new News();
                if (resultSet.next()) {
                    news.setId(resultSet.getLong(1));
                    news.setFullText(resultSet.getString(2));
                    news.setCreationDate(resultSet.getDate(3));
                    news.setModificationDate(resultSet.getDate(4));
                    news.setShortText(resultSet.getString(5));
                    news.setTitle(resultSet.getString(6));
                }
                return news;
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> getList() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (Statement statement =
                     connection.createStatement();
        ) {
            try (ResultSet resultSet = statement.executeQuery(SELECT_IN_ORDER_OF_MOST_COMMENTED_NEWS)) {
                List<News> newsList = new ArrayList<>();
                parse(resultSet, newsList);
                return newsList;
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> getEntityList(int start, int end) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_IN_ORDER_OF_MOST_COMMENTED_NEWS)
        ) {
            preparedStatement.setLong(1, start);
            preparedStatement.setLong(2, end - start);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                List<News> newsList = new ArrayList<>();
                parse(resultSet, newsList);
                return newsList;
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<News> searchByTag(Tag tag) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_NEWS_BY_TAG)
        ) {
            preparedStatement.setLong(1, tag.getId());
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                List<News> newsList = new ArrayList<>();
                parse(resultSet, newsList);
                return newsList;
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<News> searchByAuthor(Author author) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_NEWS_BY_AUTHOR)
        ) {
            preparedStatement.setLong(1, author.getId());
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                List<News> newsList = new ArrayList<>();
                parse(resultSet, newsList);
                return newsList;
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }


    @Override
    public List<News> searchByAuthorAndTag(int start, int end, SearchCriteria searchCriteria) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        StringBuilder SELECT_NEWS_BY_AUTHOR_AND_TAG = new StringBuilder();
        SELECT_NEWS_BY_AUTHOR_AND_TAG.append("SELECT NE_NEWS_ID_PK,NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE FROM NEWS LEFT JOIN COMMENTS ON NEWS.NE_NEWS_ID_PK=COMMENTS.CO_NEWS_ID_FK");
        if (searchCriteria != null) {
            if (searchCriteria.getAuthorId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" INNER JOIN NEWS_AUTHORS ON NEWS.NE_NEWS_ID_PK=NEWS_AUTHORS.NA_NEWS_ID_FK");
            }

            if (searchCriteria.getTagsId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" INNER JOIN NEWS_TAGS ON NEWS.NE_NEWS_ID_PK=NEWS_TAGS.NT_NEWS_ID_FK");
            }

            if (searchCriteria.getTagsId() != null || searchCriteria.getAuthorId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" WHERE");
            }

            if (searchCriteria.getAuthorId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" NA_AUTHOR_ID_FK=?");
            }

            if (searchCriteria.getTagsId() != null && searchCriteria.getAuthorId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" AND");
            }

            if (searchCriteria.getTagsId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" NT_TAG_ID_FK IN (?");
                for (int i = 0; i < searchCriteria.getTagsId().size() - 1; i++) {
                    SELECT_NEWS_BY_AUTHOR_AND_TAG.append(", ?");
                }
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(")");
            }
        }
        SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" GROUP BY NE_NEWS_ID_PK,NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE ORDER BY COUNT(CO_NEWS_ID_FK) DESC, NE_CREATION_DATE ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY");

        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(new String(SELECT_NEWS_BY_AUTHOR_AND_TAG))) {
            int i = 0;
            if (searchCriteria != null) {
                if (searchCriteria.getAuthorId() != null) {
                    preparedStatement.setLong(++i, searchCriteria.getAuthorId());
                }
                if (searchCriteria.getTagsId() != null) {
                    for (Long tag : searchCriteria.getTagsId()) {
                        preparedStatement.setLong(++i, tag);
                    }
                }
            }
            preparedStatement.setLong(++i, start);
            preparedStatement.setLong(++i, end - start);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                List<News> newsList = new ArrayList<>();
                parse(resultSet, newsList);
                return newsList;
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public int countNews(SearchCriteria searchCriteria) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        StringBuilder SELECT_NEWS_BY_AUTHOR_AND_TAG = new StringBuilder();
        SELECT_NEWS_BY_AUTHOR_AND_TAG.append("SELECT COUNT(NE_NEWS_ID_PK) FROM NEWS");
        if (searchCriteria != null) {
            if (searchCriteria.getAuthorId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" INNER JOIN NEWS_AUTHORS ON NEWS.NE_NEWS_ID_PK=NEWS_AUTHORS.NA_NEWS_ID_FK");

            }

            if (searchCriteria.getTagsId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" INNER JOIN NEWS_TAGS ON NEWS.NE_NEWS_ID_PK=NEWS_TAGS.NT_NEWS_ID_FK");
            }

            if (searchCriteria.getTagsId() != null || searchCriteria.getAuthorId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" WHERE");
            }

            if (searchCriteria.getAuthorId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" NA_AUTHOR_ID_FK=?");
            }

            if (searchCriteria.getTagsId() != null && searchCriteria.getAuthorId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" AND");
            }

            if (searchCriteria.getTagsId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" NT_TAG_ID_FK IN (?");
                for (int i = 0; i < searchCriteria.getTagsId().size() - 1; i++) {
                    SELECT_NEWS_BY_AUTHOR_AND_TAG.append(", ?");
                }
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(")");
            }
        }

        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(new String(SELECT_NEWS_BY_AUTHOR_AND_TAG))) {
            int i = 0;
            if (searchCriteria != null) {
                if (searchCriteria.getAuthorId() != null) {
                    preparedStatement.setLong(++i, searchCriteria.getAuthorId());
                }
                if (searchCriteria.getTagsId() != null) {
                    for (Long tag : searchCriteria.getTagsId()) {
                        preparedStatement.setLong(++i, tag);
                    }
                }
            }
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else {
                    return 0;
                }
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public int countAllNews() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(COUNT_ALL_NEWS)
        ) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else {
                    return 0;
                }
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public News getCurrentNews(SearchCriteria searchCriteria, int place) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        StringBuilder SELECT_NEWS_BY_AUTHOR_AND_TAG = new StringBuilder();
        SELECT_NEWS_BY_AUTHOR_AND_TAG.append("SELECT NE_NEWS_ID_PK,NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE FROM NEWS LEFT JOIN COMMENTS ON NEWS.NE_NEWS_ID_PK=COMMENTS.CO_NEWS_ID_FK");
        if (searchCriteria != null) {
            if (searchCriteria.getAuthorId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" INNER JOIN NEWS_AUTHORS ON NEWS.NE_NEWS_ID_PK=NEWS_AUTHORS.NA_NEWS_ID_FK");

            }

            if (searchCriteria.getTagsId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" INNER JOIN NEWS_TAGS ON NEWS.NE_NEWS_ID_PK=NEWS_TAGS.NT_NEWS_ID_FK");
            }

            if (searchCriteria.getTagsId() != null || searchCriteria.getAuthorId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" WHERE");
            }

            if (searchCriteria.getAuthorId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" NA_AUTHOR_ID_FK=?");
            }

            if (searchCriteria.getTagsId() != null && searchCriteria.getAuthorId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" AND");
            }

            if (searchCriteria.getTagsId() != null) {
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" NT_TAG_ID_FK IN (?");
                for (int i = 0; i < searchCriteria.getTagsId().size() - 1; i++) {
                    SELECT_NEWS_BY_AUTHOR_AND_TAG.append(", ?");
                }
                SELECT_NEWS_BY_AUTHOR_AND_TAG.append(")");
            }
        }
        SELECT_NEWS_BY_AUTHOR_AND_TAG.append(" GROUP BY NE_NEWS_ID_PK,NE_FULL_TEXT,NE_CREATION_DATE,NE_MODIFICATION_DATE,NE_SHORT_TEXT,NE_TITLE ORDER BY COUNT(CO_NEWS_ID_FK) DESC, NE_CREATION_DATE ASC OFFSET ? ROWS FETCH NEXT ? ROWS ONLY");
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(new String(SELECT_NEWS_BY_AUTHOR_AND_TAG))) {
            int i = 0;
            if (searchCriteria != null) {
                if (searchCriteria.getAuthorId() != null) {
                    preparedStatement.setLong(++i, searchCriteria.getAuthorId());
                }
                if (searchCriteria.getTagsId() != null) {
                    for (Long tag : searchCriteria.getTagsId()) {
                        preparedStatement.setLong(++i, tag);
                    }
                }
            }
            preparedStatement.setLong(++i, place);
            int prevNewsCount = 1;
            preparedStatement.setLong(++i, prevNewsCount);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                News news = new News();
                parse(resultSet, news);
                return news;
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    private void parse(ResultSet resultSet, List<News> newsList) throws SQLException {
        while (resultSet.next()) {
            News news = new News();
            news.setId(resultSet.getLong(1));
            news.setFullText(resultSet.getString(2));
            news.setCreationDate(resultSet.getDate(3));
            news.setModificationDate(resultSet.getDate(4));
            news.setShortText(resultSet.getString(5));
            news.setTitle(resultSet.getString(6));
            newsList.add(news);
        }
    }

    private void parse(ResultSet resultSet, News news) throws SQLException {
        if (resultSet.next()) {
            news.setId(resultSet.getLong(1));
            news.setFullText(resultSet.getString(2));
            news.setCreationDate(resultSet.getDate(3));
            news.setModificationDate(resultSet.getDate(4));
            news.setShortText(resultSet.getString(5));
            news.setTitle(resultSet.getString(6));
        }
    }

}
