package com.epam.newsmanagement.dao.impl;

import com.epam.newsmanagement.dao.ITagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
@Repository
public class TagDao implements ITagDao {
    private String UPDATE_TAG = "UPDATE TAGS SET TA_TAG_NAME = ? WHERE TA_TAG_ID_PK = ?";
    private String INSERT_INTO_TAGS = "INSERT INTO TAGS(TA_TAG_ID_PK,TA_TAG_NAME) VALUES(SEQ_TAG.NEXTVAL,?)";
    private String DELETE_TAG = "DELETE FROM TAGS WHERE TA_TAG_ID_PK = ?";
    private String SELECT_TAG_LIST = "SELECT TA_TAG_ID_PK, TA_TAG_NAME FROM TAGS";
    private String SELECT_TAG_BY_ID = "SELECT TA_TAG_ID_PK, TA_TAG_NAME FROM TAGS WHERE TA_TAG_ID_PK = ?";
    private String SELECT_TAGS_BY_NEWS_ID = "SELECT TA_TAG_ID_PK, TA_TAG_NAME FROM TAGS INNER JOIN NEWS_TAGS ON TAGS.TA_TAG_ID_PK=NEWS_TAGS.NT_TAG_ID_FK WHERE NT_NEWS_ID_FK = ?";
    private String DELETE_TAG_FROM_NEWS = "DELETE FROM NEWS_TAGS WHERE NT_TAG_ID_FK = ?";
    private String INSERT_INTO_NEWS_TAGS = "INSERT INTO NEWS_TAGS(NT_NEWS_ID_PK,NT_NEWS_ID_FK,NT_TAG_ID_FK) VALUES(SEQ_NEWS_TAG.NEXTVAL,?,?)";
    private String DELETE_FROM_NEWS_TAGS = "DELETE FROM NEWS_TAGS WHERE NT_NEWS_ID_FK = ? AND NT_TAG_ID_FK=?";
    private String ADD_TAGLIST_TO_NEWS = "INSERT INTO NEWS_TAGS (NT_NEWS_TAG_ID_PK,NT_TAG_ID_FK, NT_NEWS_ID_FK) VALUES(SEQ_NEWS_TAG.NEXTVAL,?,?)";
    private String DELETE_TAGS = "DELETE FROM NEWS_TAGS WHERE NT_NEWS_ID_FK = ?";
    @Autowired
    private DataSource dataSource;

    public TagDao() {
    }

    @Override
    public Long create(Tag tag) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        Long tagId = null;
        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(INSERT_INTO_TAGS, new String[]{"TA_TAG_ID_PK"});
        ) {
            preparedStatement.setString(1, tag.getName());
            if (preparedStatement.executeUpdate() > 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys != null && generatedKeys.next()) {
                        tagId = generatedKeys.getLong(1);
                    }
                } catch (SQLException e) {
                    throw new DaoException(e);
                }
            }
            return tagId;
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void update(Tag tag) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TAG)) {
            preparedStatement.setString(1, tag.getName());
            preparedStatement.setLong(2, tag.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void delete(Long tagId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement1 =
                        connection.prepareStatement(DELETE_TAG)) {
            preparedStatement1.setLong(1, tagId);
            preparedStatement1.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public Tag getById(Long tagId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement1 =
                        connection.prepareStatement(SELECT_TAG_BY_ID)) {
            preparedStatement1.setLong(1, tagId);
            try (ResultSet resultSet = preparedStatement1.executeQuery()) {
                Tag tag = null;
                if (resultSet.next()) {
                    tag = new Tag();
                    tag.setId(resultSet.getLong(1));
                    tag.setName(resultSet.getString(2));
                }
                return tag;
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<Tag> getList() throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement1 =
                     connection.prepareStatement(SELECT_TAG_LIST)
        ) {
            try (ResultSet resultSet = preparedStatement1.executeQuery()) {
                List<Tag> tagList = new ArrayList<>();
                Tag tag;
                while (resultSet.next()) {
                    tag = new Tag();
                    tag.setId(resultSet.getLong(1));
                    tag.setName(resultSet.getString(2));
                    tagList.add(tag);
                }
                return tagList;
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    public List<Tag> getEntityListByNewsId(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement1 =
                     connection.prepareStatement(SELECT_TAGS_BY_NEWS_ID)
        ) {
            preparedStatement1.setLong(1, newsId);
            try (ResultSet resultSet = preparedStatement1.executeQuery()) {
                List<Tag> tagList = new ArrayList<>();
                Tag tag;
                while (resultSet.next()) {
                    tag = new Tag();
                    tag.setId(resultSet.getLong(1));
                    tag.setName(resultSet.getString(2));
                    tagList.add(tag);
                }
                return tagList;
            } catch (SQLException e) {
                throw new DaoException(e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteFromAllNews(Long tagId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement1 =
                        connection.prepareStatement(DELETE_TAG_FROM_NEWS)) {
            preparedStatement1.setLong(1, tagId);
            preparedStatement1.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void addNewsTag(Long newsId, Long tagId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(INSERT_INTO_NEWS_TAGS)
        ) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.setLong(2, tagId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteNewsTag(Long newsId, Long tagId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(DELETE_FROM_NEWS_TAGS)
        ) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.setLong(2, tagId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void addNewsTagList(Long newsId, List<Tag> tagList) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement =
                     connection.prepareStatement(ADD_TAGLIST_TO_NEWS)) {

            for (Tag tag : tagList) {
                preparedStatement.setLong(1, tag.getId());
                preparedStatement.setLong(2, newsId);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public void deleteTags(Long newsId) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (
                PreparedStatement preparedStatement =
                        connection.prepareStatement(DELETE_TAGS)
        ) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
}
