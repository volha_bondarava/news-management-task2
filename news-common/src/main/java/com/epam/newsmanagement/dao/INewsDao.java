package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;
import java.util.List;

/**
 * Created by Olya Bondareva on 28/03/2015.
 */
public interface INewsDao extends IDao<News> {
    List<News> searchByTag(Tag tag) throws DaoException;
    List<News> searchByAuthor(Author author) throws DaoException;
    List<News> searchByAuthorAndTag(int start, int end, SearchCriteria searchCriteria) throws DaoException;
    int countNews(SearchCriteria searchCriteria) throws DaoException;
    List<News> getEntityList(int start, int ende) throws DaoException;
    int countAllNews() throws DaoException;
    News getCurrentNews(SearchCriteria searchCriteria, int place) throws DaoException;
}
