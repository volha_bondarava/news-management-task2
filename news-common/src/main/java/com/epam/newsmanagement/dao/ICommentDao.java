package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;

import java.util.List;

/**
 * Created by Olya Bondareva on 29/03/2015.
 */
public interface ICommentDao extends IDao<Comment> {
    void deleteNewsComment(Long newsId, Long commentId) throws DaoException;
    List<Comment> getEntityListByNewsId(Long newsId) throws DaoException;
    void deleteNewsComments(Long newsId) throws DaoException;
}
