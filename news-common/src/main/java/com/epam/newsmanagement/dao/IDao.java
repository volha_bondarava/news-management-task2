package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Entity;
import com.epam.newsmanagement.exception.DaoException;

import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
public interface IDao<T extends Entity> {
    public Long create(T entity) throws DaoException;
    public void update(T entity) throws DaoException;
    public void delete(Long id) throws DaoException;
    public T getById(Long id) throws DaoException;
    public List<T> getList() throws DaoException;
}
