package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.dao.ICommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
@Service
public class CommentService implements ICommentService {
    private static Logger logger = Logger.getLogger(CommentService.class);
    @Autowired
    private ICommentDao iCommentDao;

    public CommentService() {
    }

    public CommentService(ICommentDao iCommentDao) {
        this.iCommentDao = iCommentDao;
    }

    @Override
    public long addComment(Comment comment) throws ServiceException {
        try {
            return iCommentDao.create(comment);
        } catch (DaoException e) {
            logger.error("Error during creating comment",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteComment(Long commentId) throws ServiceException {
        Comment commentFromDatabase;
        try {
            commentFromDatabase = iCommentDao.getById(commentId);
            if (commentFromDatabase != null) {
                iCommentDao.delete(commentId);
            }
            else {
                logger.error("no such comment in storage");
                throw new ServiceException("no such comment in storage");
            }
        } catch (DaoException e) {
            logger.error("Error during deleting comment",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteNewsComment(Long newsId, Long commentId) throws ServiceException {
        try {
            iCommentDao.deleteNewsComment(newsId, commentId);
        } catch (DaoException e) {
            logger.error("Error during deleting comment from news",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Comment> getCommentListByNewsId(Long newsId) throws ServiceException {
        try {
            return iCommentDao.getEntityListByNewsId(newsId);
        } catch (DaoException e) {
            logger.error("Error during getting comment by news id",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteNewsComments(Long newsId) throws ServiceException{
        try {
            iCommentDao.deleteNewsComments(newsId);
        } catch (DaoException e) {
            logger.error("Error during deleting comment from news",e);
            throw new ServiceException(e);
        }
    }

}
