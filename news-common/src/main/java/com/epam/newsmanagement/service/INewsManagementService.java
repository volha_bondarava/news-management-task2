package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.FilteredNews;
import com.epam.newsmanagement.entity.NewsDTO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Created by Olya Bondareva on 04/04/2015.
 */
public interface INewsManagementService {
    FilteredNews getFilteredDTOList(int start, int end, SearchCriteria searchCriteria) throws ServiceException;

    NewsDTO getNewsDTOByNewsId(Long newsId) throws ServiceException;

    void deleteNews(Long newsId) throws  ServiceException;

    void addNewsDTO(NewsDTO newsDTO) throws ServiceException;

    void updateNewsDTO(NewsDTO newsDTO) throws ServiceException;

    NewsDTO getCurrentNewsDTO(SearchCriteria searchCriteria, int place) throws ServiceException;

}
