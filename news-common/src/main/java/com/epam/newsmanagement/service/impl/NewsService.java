package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.INewsDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.service.INewsService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
@Service
public class NewsService implements INewsService {
    private static Logger logger = Logger.getLogger(NewsService.class);
    @Autowired
    private INewsDao iNewsDao;

    public NewsService() {
    }

    @Autowired
    public NewsService(INewsDao iNewsDao) {
        this.iNewsDao = iNewsDao;
    }

    @Override
    public Long addNews(News news) throws ServiceException {
        try {
            return iNewsDao.create(news);
        } catch (DaoException e) {
            logger.error("Error during creating news", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void updateNews(News news) throws ServiceException {
        News newsFromDatabase;
        try {
            newsFromDatabase = iNewsDao.getById(news.getId());
            if (newsFromDatabase != null) {
                iNewsDao.update(news);
            }
            else {
                logger.error("no such news in storage");
                throw new ServiceException("no such news in storage");
            }
        } catch (DaoException e) {
            logger.error("Error during updating news",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteNews(Long newsId) throws ServiceException {
        News newsFromDatabase;
        try {
            newsFromDatabase = iNewsDao.getById(newsId);
            if (newsFromDatabase != null) {
                iNewsDao.delete(newsId);
            }
            else {
                throw new ServiceException("no such news in storage");
            }
        } catch (DaoException e) {
            logger.error("Error during deleting news",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> viewNewsList(int start, int end) throws ServiceException {
        try {
            return iNewsDao.getEntityList(start, end);
        } catch (DaoException e) {
            logger.error("Error during getting the list of news",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public News viewSingleNews(Long newsId) throws ServiceException {
        try {
            return iNewsDao.getById(newsId);
        } catch (DaoException e) {
            logger.error("Error during getting single news",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> searchByTag(Tag tag) throws ServiceException {
        try {
            return iNewsDao.searchByTag(tag);
        } catch (DaoException e) {
            logger.error("Error during searching by tag",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> searchByAuthor(Author author) throws ServiceException {
        try {
            return iNewsDao.searchByAuthor(author);
        } catch (DaoException e) {
            logger.error("Error during searching by author",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> searchByAuthorAndTag(int start, int end, SearchCriteria searchCriteria) throws ServiceException {
        try {
            return iNewsDao.searchByAuthorAndTag(start, end, searchCriteria);
        } catch (DaoException e) {
            logger.error("Error during searching by author",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public int countNews(SearchCriteria searchCriteria) throws ServiceException {
        try {
            return iNewsDao.countNews(searchCriteria);
        } catch (DaoException e) {
            logger.error("Error during counting news",e);
            throw new ServiceException(e);
        }
    }


    @Override
    public int countAllNews() throws ServiceException {
        try {
            return iNewsDao.countAllNews();
        } catch (DaoException e) {
            logger.error("Error during counting news",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public News viewCurrentNews(SearchCriteria searchCriteria, int place) throws ServiceException {
        try {
            return iNewsDao.getCurrentNews(searchCriteria, place);
        } catch (DaoException e) {
            logger.error("Error during getting single news",e);
            throw new ServiceException(e);
        }
    }


}
