package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
public interface INewsService {
    Long addNews(News news) throws ServiceException;

    void updateNews(News news) throws ServiceException;

    void deleteNews(Long newsId) throws ServiceException;

    List<News> viewNewsList(int start, int end) throws ServiceException;

    News viewSingleNews(Long newsId) throws ServiceException;

    List<News> searchByTag(Tag tag) throws ServiceException;

    List<News> searchByAuthor(Author author) throws ServiceException;

    List<News> searchByAuthorAndTag(int start, int end, SearchCriteria searchCriteria) throws ServiceException;

    int countNews(SearchCriteria searchCriteria) throws ServiceException;

    int countAllNews() throws  ServiceException;

    News viewCurrentNews(SearchCriteria searchCriteria, int place) throws ServiceException;
}
