package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.ITagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ITagService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
@Service
public class TagService implements ITagService {
    private static Logger logger = Logger.getLogger(TagService.class);
    @Autowired
    private ITagDao iTagDao;

    public TagService() {
    }

    public TagService(ITagDao iTagDao) {
        this.iTagDao = iTagDao;
    }

    @Override
    public long addTag(Tag tag) throws ServiceException {
        try {
            return iTagDao.create(tag);
        } catch (DaoException e) {
            logger.error("Error during creating tag",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteTag(Long tagId) throws ServiceException {
        Tag tagFromDatabase;
        try {
            tagFromDatabase = iTagDao.getById(tagId);
            if (tagFromDatabase != null) {
                iTagDao.deleteFromAllNews(tagId);
                iTagDao.delete(tagId);
            }
            else {
                throw new ServiceException("no such tag in storage");
            }
        } catch (DaoException e) {
            logger.error("Error during deleting tag",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void updateTag(Tag tag) throws ServiceException {
        Tag tagFromDatabase;
        try {
            tagFromDatabase = iTagDao.getById(tag.getId());
            if (tagFromDatabase != null) {
                iTagDao.update(tag);
            }
            else {
                throw new ServiceException("no such tag in storage");
            }
        } catch (DaoException e) {
            logger.error("Error during deleting tag",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Tag> getTagListByNewsId(Long newsId) throws ServiceException {
        try {
            return iTagDao.getEntityListByNewsId(newsId);
        } catch (DaoException e) {
            logger.error("Error during getting tag by news id",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Tag> getTagList() throws ServiceException {
        try {
            return iTagDao.getList();
        } catch (DaoException e) {
            logger.error("Error during getting tag by news id",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Tag getTagById(Long tagId) throws ServiceException {
        try {
            return iTagDao.getById(tagId);
        } catch (DaoException e) {
            logger.error("Error during getting tag by news id",e);
            throw new ServiceException(e);
        }
    }


    @Override
    public void addNewsTagList(Long newsId, List<Tag> tagList) throws ServiceException {
        try {
            iTagDao.addNewsTagList(newsId, tagList);
        } catch (DaoException e) {
            logger.error("Error during adding tag to news",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void addNewsTag(Long newsId, Long tagId) throws ServiceException {
        try {
            iTagDao.addNewsTag(newsId, tagId);
        } catch (DaoException e) {
            logger.error("Error during adding tag to news",e);
            throw new ServiceException(e);
        }
    }
    @Override
    public void deleteNewsTag(Long newsId, Long tagId) throws ServiceException {
        try {
            iTagDao.deleteNewsTag(newsId, tagId);
        } catch (DaoException e) {
            logger.error("Error during deleting tag from news",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteNewsTag(Long newsId) throws ServiceException {
        try {
            iTagDao.deleteTags(newsId);
        } catch (DaoException e) {
            logger.error("Error during getting tag by news id",e);
            throw new ServiceException(e);
        }
    }
}
