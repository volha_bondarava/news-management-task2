package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.Date;
import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
public interface IAuthorService {
    long addAuthor(Author author) throws ServiceException;

    void addAuthorNews(Long authorId, Long newsId) throws ServiceException;

    void deleteAuthorNews(Long newsId) throws ServiceException;

    void deleteAuthor(Long authorId) throws ServiceException;

    Author getNewsAuthor(Long newsId) throws ServiceException;

    List<Author> getAuthorList() throws ServiceException;

    void expireAuthor(Long authorId, Date expiredDate) throws ServiceException;

    void updateAuthor(Author author) throws ServiceException;

    Author getAuthorById(Long authorId) throws ServiceException;
}
