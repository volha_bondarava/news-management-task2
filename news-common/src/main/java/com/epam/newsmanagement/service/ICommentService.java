package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
public interface ICommentService {
    long addComment(Comment comment) throws ServiceException;

    void deleteComment(Long commentId) throws ServiceException;

    void deleteNewsComment(Long newsId, Long commentId) throws ServiceException;

    List<Comment> getCommentListByNewsId(Long newsId) throws ServiceException;

    void deleteNewsComments(Long newsId) throws ServiceException;
}
