package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.IAuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IAuthorService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
@Service
public class AuthorService implements IAuthorService {
    private static Logger logger = Logger.getLogger(AuthorService.class);
    @Autowired
    private IAuthorDao iAuthorDao;

    public AuthorService() {
    }

    public AuthorService(IAuthorDao iAuthorDao) {
        this.iAuthorDao = iAuthorDao;
    }

    @Override
    public long addAuthor(Author author) throws ServiceException {
        try {
            return iAuthorDao.create(author);
        } catch (DaoException e) {
            logger.error("Error during creating author", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void addAuthorNews(Long authorId, Long newsId) throws ServiceException {
        try {
            iAuthorDao.addAuthorNews(authorId, newsId);
        } catch (DaoException e) {
            logger.error("Error during addition news to author", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteAuthorNews(Long newsId) throws ServiceException {
        try {
            iAuthorDao.deleteAuthorNews(newsId);
        } catch (DaoException e) {
            logger.error("Error during deleting news from author", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteAuthor(Long authorId) throws ServiceException {
        try {
            iAuthorDao.delete(authorId);
        } catch (DaoException e) {
            logger.error("Error during deleting news from author", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Author getNewsAuthor(Long newsId) throws ServiceException {
        try {
            return iAuthorDao.getNewsAuthor(newsId);
        } catch (DaoException e) {
            logger.error("Error during getting author by news id",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Author> getAuthorList() throws ServiceException {
        try {
            return iAuthorDao.getList();
        } catch (DaoException e) {
            logger.error("Error during getting comment by news id",e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void expireAuthor(Long authorId, Date expiredDate) throws ServiceException {
        try {
            iAuthorDao.expireAuthor(authorId, expiredDate);
        } catch (DaoException e) {
            logger.error("Error during deleting news from author", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void updateAuthor(Author author) throws ServiceException {
        try {
            iAuthorDao.update(author);
        } catch (DaoException e) {
            logger.error("Error during deleting news from author", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public Author getAuthorById(Long authorId) throws ServiceException {
        try {
            return iAuthorDao.getById(authorId);
        } catch (DaoException e) {
            logger.error("Error during getting author", e);
            throw new ServiceException(e);
        }
    }
}
