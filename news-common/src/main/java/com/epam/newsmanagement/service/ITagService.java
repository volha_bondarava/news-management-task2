package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * Created by Olya Bondareva on 25/03/2015.
 */
public interface ITagService {
    long addTag(Tag tag) throws ServiceException;

    void deleteTag(Long tagId) throws ServiceException;

    void updateTag(Tag tag) throws ServiceException;

    List<Tag> getTagListByNewsId(Long newsId) throws ServiceException;

    List<Tag> getTagList() throws ServiceException;

    Tag getTagById(Long tagId) throws  ServiceException;

    void addNewsTag(Long newsId, Long tagId) throws ServiceException;

    void deleteNewsTag(Long newsId, Long tagId) throws ServiceException;

    void addNewsTagList(Long newsId, List<Tag> tagList) throws ServiceException;

    void deleteNewsTag(Long newsId) throws  ServiceException;
}
