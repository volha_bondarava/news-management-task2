package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.entity.FilteredNews;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsDTO;
import com.epam.newsmanagement.entity.SearchCriteria;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Olya Bondareva on 04/04/2015.
 */
@Service
public class NewsManagementService implements INewsManagementService {
    @Autowired
    private INewsService iNewsService;
    @Autowired
    private ITagService iTagService;
    @Autowired
    private IAuthorService iAuthorService;
    @Autowired
    private ICommentService iCommentService;


    public NewsManagementService() {
    }

    public INewsService getINewsService() {
        return iNewsService;
    }

    public void setINewsService(INewsService iNewsService) {
        this.iNewsService = iNewsService;
    }

    public ITagService getITagService() {
        return iTagService;
    }

    public void setITagService(ITagService iTagService) {
        this.iTagService = iTagService;
    }

    public IAuthorService getIAuthorService() {
        return iAuthorService;
    }

    public void setIAuthorService(IAuthorService iAuthorService) {
        this.iAuthorService = iAuthorService;
    }

    public ICommentService getICommentService() {
        return iCommentService;
    }

    public void setICommentService(ICommentService iCommentService) {
        this.iCommentService = iCommentService;
    }

    @Override
    @Transactional
    public FilteredNews getFilteredDTOList(int start, int end, SearchCriteria searchCriteria) throws ServiceException {
        FilteredNews filteredNews = new FilteredNews();
        List<News> newsList = iNewsService.searchByAuthorAndTag(start, end, searchCriteria);
        List<NewsDTO> newsDTOList = new ArrayList<>();
        int i = 0;
        for (News news : newsList) {
            NewsDTO newsDTO = new NewsDTO();
            Long newsId = news.getId();
            newsDTO.setNews(news);
            newsDTO.setAuthor(iAuthorService.getNewsAuthor(newsId));
            newsDTO.setTagList(iTagService.getTagListByNewsId(newsId));
            newsDTO.setCommentList(iCommentService.getCommentListByNewsId(newsId));
            newsDTOList.add(i, newsDTO);
            ++i;
        }
        filteredNews.setNewsDTOList(newsDTOList);
        filteredNews.setMatchedCount(iNewsService.countNews(searchCriteria));
        return filteredNews;
    }

    @Override
    @Transactional
    public NewsDTO getNewsDTOByNewsId(Long newsId) throws ServiceException {
        NewsDTO newsDTO = new NewsDTO();
        News news = iNewsService.viewSingleNews(newsId);
        newsDTO.setNews(news);
        newsDTO.setAuthor(iAuthorService.getNewsAuthor(newsId));
        newsDTO.setTagList(iTagService.getTagListByNewsId(newsId));
        newsDTO.setCommentList(iCommentService.getCommentListByNewsId(newsId));
        return newsDTO;
    }

    @Override
    @Transactional
    public void deleteNews(Long newsId) throws ServiceException {
        iAuthorService.deleteAuthorNews(newsId);
        iCommentService.deleteNewsComments(newsId);
        iTagService.deleteNewsTag(newsId);
        iNewsService.deleteNews(newsId);
    }

    @Override
    @Transactional
    public void addNewsDTO(NewsDTO newsDTO) throws ServiceException {
        newsDTO.getNews().setId(iNewsService.addNews(newsDTO.getNews()));
        iAuthorService.addAuthorNews(newsDTO.getAuthor().getId(), newsDTO.getNews().getId());
        iTagService.addNewsTagList(newsDTO.getNews().getId(), newsDTO.getTagList());
    }

    @Override
    @Transactional
    public void updateNewsDTO(NewsDTO newsDTO) throws ServiceException {
        Long newsId = newsDTO.getNews().getId();
        iAuthorService.deleteAuthorNews(newsId);
        iTagService.deleteNewsTag(newsId);
        iNewsService.updateNews(newsDTO.getNews());
        iAuthorService.addAuthorNews(newsDTO.getAuthor().getId(), newsId);
        iTagService.addNewsTagList(newsId, newsDTO.getTagList());
    }

    @Override
    public NewsDTO getCurrentNewsDTO(SearchCriteria searchCriteria, int place) throws ServiceException {
        NewsDTO newsDTO = new NewsDTO();
        News news = iNewsService.viewCurrentNews(searchCriteria, place);
        Long newsId = news.getId();
        newsDTO.setNews(news);
        newsDTO.setAuthor(iAuthorService.getNewsAuthor(newsId));
        newsDTO.setTagList(iTagService.getTagListByNewsId(newsId));
        newsDTO.setCommentList(iCommentService.getCommentListByNewsId(newsId));
        return newsDTO;
    }
}
